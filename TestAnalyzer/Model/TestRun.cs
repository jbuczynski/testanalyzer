﻿namespace Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.Serialization;

    using Enums;

    [DataContract]
    public class TestRun : Base
    {
        [DataMember]
        [ForeignKey("TestCycleId")]
        public TestCycle TestCycle { get; set; }
        
        [DataMember]
        public virtual List<TestMethod> TestsMethods { get; set; }

        [DataMember]
        public UploadType UploadType { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public ApplicationUser User { get; set; }

        [DataMember]
        public int TestCycleId { get; set; }
    }
}
