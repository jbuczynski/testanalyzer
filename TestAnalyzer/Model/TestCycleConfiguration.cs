﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    [DataContract]
    public class TestCycleConfiguration : Base
    {
        [DataMember]
        public TestCycle DefaultTestCycle { get; set; }

        [DataMember]
        public ApplicationUser User { get; set; }
    }
}
