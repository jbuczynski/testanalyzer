﻿using System;

namespace Model
{
    using System.Runtime.Serialization;

    [DataContract]
    public class Base
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime Added { get; set; }
        [DataMember]
        public DateTime? Modified { get; set; }
    }
}
