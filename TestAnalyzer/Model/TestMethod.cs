﻿using Model.Enums;

using System.Collections.Generic;

namespace Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.Serialization;

    [DataContract]
    public class TestMethod : Base
    {
        [IgnoreDataMember]
        [ForeignKey("TestRunId")]
        public virtual TestRun TestRun { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public virtual List<MethodParameter> MethodParameters { get; set; }

        [DataMember]
        public Result Result { get; set; }

        [DataMember]
        public string ClassName { get; set; }

        [DataMember]
        public string ErrorMsg { get; set; }

        [DataMember]
        public double Duration { get; set; }

        [DataMember]
        public int TestRunId { get; set; }
    }
}
