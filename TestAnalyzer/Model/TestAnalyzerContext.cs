﻿namespace Model
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;

    using Microsoft.AspNet.Identity.EntityFramework;

    /// <summary>
    /// The test analyzer context.
    /// </summary>
    public class TestAnalyzerContext : IdentityDbContext<ApplicationUser>
    {
        public static System.Data.Entity.SqlServer.SqlProviderServices EnsureAssemblySqlServerIsCopied { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestAnalyzerContext"/> class.
        /// </summary>
        public TestAnalyzerContext() : base("DefaultConnection")
        {
            Database.SetInitializer<TestAnalyzerContext>(new TestAnalyzerInitializerIfModelChanges());
        }

        [Obsolete("This connetext should be used only for test.")]
        public TestAnalyzerContext(string connectionName) : base(connectionName)
        {
            Database.SetInitializer<TestAnalyzerContext>(new DropCreateDatabaseIfModelChanges<TestAnalyzerContext>());
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ApplicationDbContext"/>.
        /// </returns>
        public static TestAnalyzerContext Create()
        {
            return new TestAnalyzerContext();
        }

        public virtual DbSet<MethodParameter> MethodParameters { get; set; }
        public virtual DbSet<OriginalInput> OriginalInputs { get; set; }
        public virtual DbSet<TestCycle> TestCycles { get; set; }
        public virtual DbSet<TestCycleConfiguration> TestCycleConfigurations { get; set; }
        public virtual DbSet<TestMethod> TestMethods { get; set; }
        public virtual DbSet<TestRun> TestRuns { get; set; }

        /// <summary>
        /// The save changes.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public override int SaveChanges()
        {
            var modifiedEntries = this.ChangeTracker.Entries()
                .Where(x => x.Entity is Base
                    && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                Base entity = entry.Entity as Base;
                if (entity != null)
                {
                    string identityName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime now = DateTime.UtcNow;

                    if (entry.State == System.Data.Entity.EntityState.Added)
                    {
                        entity.Added = now;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.Modified).IsModified = false;
                    }

                    entity.Modified = now;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUserLogin>().HasKey<string>(l => l.UserId);
            modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id);
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
        }
    }
}
