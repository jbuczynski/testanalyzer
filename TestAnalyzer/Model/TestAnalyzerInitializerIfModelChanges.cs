﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Model
{
    using Model.Enums;
    using System.Collections.Generic;
    public class TestAnalyzerInitializerIfModelChanges : DropCreateDatabaseIfModelChanges<TestAnalyzerContext>
    {
        protected override void Seed(TestAnalyzerContext context)
        {
            AddRecordToDb(context);

            base.Seed(context);
        }

        public static void AddRecordToDb(TestAnalyzerContext context)
        {
            #region Users

            UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            ApplicationUser user1 = new ApplicationUser
            {
                Id = "user1",
                Email = "user1@email.com",
                UserName = "user1@email.com",
                EmailConfirmed = true
            };
            manager.Create(user1, "zaq1@WSX");

            ApplicationUser user2 = new ApplicationUser
            {
                Id = "user2",
                Email = "user2@email.com",
                UserName = "user2@email.com",
                EmailConfirmed = true
            };
            manager.Create(user2, "zaq1@WSX");

            ApplicationUser user3 = new ApplicationUser
            {
                Id = "user3",
                Email = "user3@email.com",
                UserName = "user3@email.com",
                EmailConfirmed = true
            };
            manager.Create(user3, "zaq1@WSX");

            var user4 = new ApplicationUser
            {
                Id = "user4",
                Email = "user4@email.com",
                UserName = "user4@email.com",
                EmailConfirmed = true
            };
            manager.Create(user4, "zaq1@WSX");

            #endregion

            #region TestCycles

            TestCycle testCycle1 = new TestCycle()
            {
                Id = 1,
                Comment = "TestCycle 1",
                IsApproved = true,
                Name = "TestCycle 1",
                User = user1
            };
            context.TestCycles.AddOrUpdate(testCycle1);

            TestCycle testCycle2 = new TestCycle()
            {
                Id = 2,
                Comment = "TestCycle 2",
                IsApproved = true,
                Name = "TestCycle 2",
                User = user2
            };
            context.TestCycles.AddOrUpdate(testCycle2);

            TestCycle testCycle3 = new TestCycle()
            {
                Id = 3,
                Comment = "TestCycle 3",
                IsApproved = true,
                Name = "TestCycle 3",
                User = user2
            };
            context.TestCycles.AddOrUpdate(testCycle3);

            #endregion

            #region TestRuns

            TestRun testRun1 = new TestRun
            {
                UploadType = UploadType.Ci,
                Comment = "Tesr run comment 1",
                User = user1,
                TestCycle = testCycle1,
            };
            context.TestRuns.Add(testRun1);

            TestRun testRun2 = new TestRun
            {
                UploadType = UploadType.Ci,
                Comment = "Tesr run comment 2",
                User = user2,
                TestCycle = testCycle2,
            };
            context.TestRuns.Add(testRun2);

            TestRun testRun3 = new TestRun
            {
                UploadType = UploadType.User,
                Comment = "Tesr run comment 3",
                User = user2,
                TestCycle = testCycle2,
            };
            context.TestRuns.Add(testRun3);

            TestRun testRun4 = new TestRun
            {
                UploadType = UploadType.User,
                Comment = "Tesr run comment 4 (User2)",
                User = user2,
                TestCycle = testCycle3,
            };
            context.TestRuns.Add(testRun4);

            #endregion

            #region TestMethods

            List<MethodParameter> methodParameters1 = new List<MethodParameter> {
                new MethodParameter
                {
                    Name = "ParamA",
                    Value = "StringTest1"
                },
                new MethodParameter
                {
                    Name = "ParamB",
                    Value = "StringTest2"
                }
            };

            List<MethodParameter> methodParameters2 = new List<MethodParameter> {
                new MethodParameter
                {
                    Name = "ParamC",
                    Value = "StringTest3"
                },
                new MethodParameter
                {
                    Name = "ParamD",
                    Value = "StringTest4"
                }
            };

            TestMethod testMethod1 = new TestMethod
            {
                TestRun = testRun2,
                Name = "MethodName1",
                MethodParameters = methodParameters1,
                Result = Result.Pass,
                ClassName = "Some.Name.Space",
                Duration = 0.003
            };
            context.TestMethods.Add(testMethod1);

            TestMethod testMethod2 = new TestMethod
            {
                TestRun = testRun2,
                Name = "MethodName2",
                MethodParameters = methodParameters2,
                Result = Result.Pass,
                ClassName = "Other.Name.Space",
                Duration = 56.003
            };
            context.TestMethods.Add(testMethod2);

            TestMethod testMethod3 = new TestMethod
            {
                TestRun = testRun3,
                Name = "MethodName1",
                MethodParameters = methodParameters1,
                Result = Result.Pass,
                ClassName = "Some.Name.Space",
                Duration = 0.010
            };
            context.TestMethods.Add(testMethod1);

            TestMethod testMethod4 = new TestMethod
            {
                TestRun = testRun3,
                Name = "MethodName2",
                MethodParameters = methodParameters2,
                Result = Result.Fail,
                ClassName = "Other.Name.Space",
                Duration = 600.434
            };
            context.TestMethods.Add(testMethod2);

            #endregion

            #region Test Data from excel file

            TestCycle testCycle11 = new TestCycle()
            {
                Comment = "TestCycle 11",
                IsApproved = true,
                Name = "TestCycle 11",
                User = user2
            };
            context.TestCycles.AddOrUpdate(testCycle11);

            TestCycle testCycle22 = new TestCycle()
            {
                Comment = "TestCycle 22",
                IsApproved = true,
                Name = "TestCycle 22",
                User = user2
            };
            context.TestCycles.AddOrUpdate(testCycle22);

            TestRun testRun11 = new TestRun
            {
                UploadType = UploadType.Ci,
                Comment = "Tesr run comment 11",
                User = user2,
                TestCycle = testCycle11,
            };
            context.TestRuns.Add(testRun11);

            TestRun testRun22 = new TestRun
            {
                UploadType = UploadType.Ci,
                Comment = "Tesr run comment 22",
                User = user2,
                TestCycle = testCycle11,
            };
            context.TestRuns.Add(testRun22);

            TestRun testRun33 = new TestRun
            {
                UploadType = UploadType.Ci,
                Comment = "Tesr run comment 33",
                User = user2,
                TestCycle = testCycle22,
            };
            context.TestRuns.Add(testRun33);

            #region testRun11

            TestMethod testMethod1TC11TR11 = new TestMethod
            {
                TestRun = testRun11,
                ClassName = "Some.namespace",
                Name = "convertToRome",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "a",
                            Value = "5"
                        }
                    },
                Result = Result.Pass,
                Duration = 23.04
            };
            context.TestMethods.Add(testMethod1TC11TR11);

            TestMethod testMethod2TC11TR11 = new TestMethod
            {
                TestRun = testRun11,
                ClassName = "other.namespace",
                Name = "convertToGreek",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "a",
                            Value = "X"
                        }
                    },
                Result = Result.Pass,
                Duration = 14.04
            };
            context.TestMethods.Add(testMethod2TC11TR11);

            TestMethod testMethod3TC11TR11 = new TestMethod
            {
                TestRun = testRun11,
                ClassName = "nice.namespace",
                Name = "failingMethod",
                Result = Result.Fail,
                Duration = 0.2
            };
            context.TestMethods.Add(testMethod3TC11TR11);

            TestMethod testMethod4TC11TR11 = new TestMethod
            {
                TestRun = testRun11,
                ClassName = "some.namespace",
                Name = "convertToEnglish",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "text",
                            Value = "Kanapka"
                        }
                    },
                Result = Result.Pass,
                Duration = 2.56
            };
            context.TestMethods.Add(testMethod4TC11TR11);

            TestMethod testMethod5TC11TR11 = new TestMethod
            {
                TestRun = testRun11,
                ClassName = "some.namespace",
                Name = "checkIfNoError",
                Result = Result.Fail,
                Duration = 23.04
            };
            context.TestMethods.Add(testMethod5TC11TR11);

            TestMethod testMethod6TC11TR11 = new TestMethod
            {
                TestRun = testRun11,
                ClassName = "next.namespace.com",
                Name = "awesomeMethod",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "digit",
                            Value = "5"
                        }
                    },
                Result = Result.Pass,
                Duration = 1.07
            };
            context.TestMethods.Add(testMethod6TC11TR11);

            TestMethod testMethod7TC11TR11 = new TestMethod
            {
                TestRun = testRun11,
                ClassName = "next.namespace.com",
                Name = "awesomeMethod",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "digit",
                            Value = "10"
                        }
                    },
                Result = Result.Fail,
                Duration = 7.44
            };
            context.TestMethods.Add(testMethod7TC11TR11);

            #endregion

            #region testRun22

            TestMethod testMethod1TC11TR22 = new TestMethod
            {
                TestRun = testRun22,
                ClassName = "Some.namespace",
                Name = "convertToRome",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "a",
                            Value = "5"
                        }
                    },
                Result = Result.Pass,
                Duration = 19.43
            };
            context.TestMethods.Add(testMethod1TC11TR22);

            TestMethod testMethod2TC11TR22 = new TestMethod
            {
                TestRun = testRun22,
                ClassName = "other.namespace",
                Name = "convertToGreek",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "a",
                            Value = "X"
                        }
                    },
                Result = Result.Pass,
                Duration = 17.95
            };
            context.TestMethods.Add(testMethod2TC11TR22);

            TestMethod testMethod3TC11TR22 = new TestMethod
            {
                TestRun = testRun22,
                ClassName = "nice.namespace",
                Name = "failingMethod",
                Result = Result.Fail,
                Duration = 0.3
            };
            context.TestMethods.Add(testMethod3TC11TR22);

            TestMethod testMethod4TC11TR22 = new TestMethod
            {
                TestRun = testRun22,
                ClassName = "some.namespace",
                Name = "convertToEnglish",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "text",
                            Value = "Kanapka"
                        }
                    },
                Result = Result.Fail,
                Duration = 98.43
            };
            context.TestMethods.Add(testMethod4TC11TR22);

            TestMethod testMethod5TC11TR22 = new TestMethod
            {
                TestRun = testRun22,
                ClassName = "some.namespace",
                Name = "checkIfNoError",
                Result = Result.Pass,
                Duration = 33.43
            };
            context.TestMethods.Add(testMethod5TC11TR22);

            TestMethod testMethod6TC11TR22 = new TestMethod
            {
                TestRun = testRun22,
                ClassName = "right.namespace.com",
                Name = "prepareData",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "value",
                            Value = "1"
                        }
                    },
                Result = Result.Pass,
                Duration = 4.04
            };
            context.TestMethods.Add(testMethod6TC11TR22);

            TestMethod testMethod7TC11TR22 = new TestMethod
            {
                TestRun = testRun22,
                ClassName = "right.namespace.com",
                Name = "prepareData",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "value",
                            Value = "5"
                        }
                    },
                Result = Result.Fail,
                Duration = 32.03
            };
            context.TestMethods.Add(testMethod7TC11TR22);

            #endregion

            #region TestRun33

            TestMethod testMethod1TC22TR33 = new TestMethod
            {
                TestRun = testRun33,
                ClassName = "Some.namespace",
                Name = "convertToRome",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "a",
                            Value = "5"
                        }
                    },
                Result = Result.Pass,
                Duration = 19.44
            };
            context.TestMethods.Add(testMethod1TC22TR33);

            TestMethod testMethod2TC22TR33 = new TestMethod
            {
                TestRun = testRun33,
                ClassName = "nice.namespace",
                Name = "failingMethod",
                Result = Result.Fail,
                Duration = 0.4
            };
            context.TestMethods.Add(testMethod2TC22TR33);

            TestMethod testMethod3TC22TR33 = new TestMethod
            {
                TestRun = testRun33,
                ClassName = "some.namespace",
                Name = "convertToEnglish",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "text",
                            Value = "Kanapka"
                        }
                    },
                Result = Result.Pass,
                Duration = 2.56
            };
            context.TestMethods.Add(testMethod3TC22TR33);

            TestMethod testMethod4TC22TR33 = new TestMethod
            {
                TestRun = testRun33,
                ClassName = "some.namespace",
                Name = "checkIfNoError",
                Result = Result.Pass,
                Duration = 77.57
            };
            context.TestMethods.Add(testMethod4TC22TR33);

            TestMethod testMethod5TC22TR33 = new TestMethod
            {
                TestRun = testRun33,
                ClassName = "right.namespace.com",
                Name = "sendEmailTo",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "email",
                            Value = "test@mail.com"
                        }
                    },
                Result = Result.Pass,
                Duration = 4.05
            };
            context.TestMethods.Add(testMethod5TC22TR33);

            TestMethod testMethod6TC22TR33 = new TestMethod
            {
                TestRun = testRun33,
                ClassName = "right.namespace.com",
                Name = "sendEmailTo",
                MethodParameters = new List<MethodParameter> {
                    new MethodParameter
                        {
                            Name = "email",
                            Value = "wrong@mail@.com"
                        }
                    },
                Result = Result.Fail,
                Duration = 32.04
            };
            context.TestMethods.Add(testMethod6TC22TR33);

            #endregion

            #endregion
        }
    }
}
