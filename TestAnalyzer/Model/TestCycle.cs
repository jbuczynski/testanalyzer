﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    [DataContract]
    public class TestCycle : Base
    {
        [DataMember]
        public virtual List<TestRun> TestsRuns { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public ApplicationUser User { get; set; }

        [DataMember]
        public bool IsApproved { get; set; }
    }
}
