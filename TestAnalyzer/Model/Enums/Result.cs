﻿namespace Model.Enums
{
    using System.Runtime.Serialization;

    [DataContract]
    public enum Result
    {
        [EnumMember]
        Pass,

        [EnumMember]
        Fail,

        [EnumMember]
        Inconclusive,

        [EnumMember]
        Blocked,

        [EnumMember]
        Error,

        [EnumMember]
        NotRun,

        [EnumMember]
        Ignored,
    }
}
