﻿namespace Model.Enums
{
    using System.Runtime.Serialization;

    [DataContract]
    public enum TestSuitsType
    {
        [EnumMember]
        JUnit,
        
        [EnumMember]
        NUnit,

        [EnumMember]
        Unknow
    }
}
