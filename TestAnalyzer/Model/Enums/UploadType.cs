﻿namespace Model.Enums
{
    using System.Runtime.Serialization;

    [DataContract]
    public enum UploadType
    {
        [EnumMember]
        User,

        [EnumMember]
        Ci
    }
}
