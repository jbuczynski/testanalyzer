﻿namespace Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration.Configuration;
    using System.Runtime.Serialization;

    [DataContract]
    public class MethodParameter : Base
    {
        [IgnoreDataMember]
        [ForeignKey("TestMethodId")]
        public virtual TestMethod TestMethod { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public int TestMethodId { get; set; }
    }
}
