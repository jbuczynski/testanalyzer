﻿namespace Model
{
    using System.Runtime.Serialization;

    [DataContract]
    public class OriginalInput : Base
    {
        [DataMember]
        public int TestRunId { get; set; }

        [DataMember]
        public string Xml { get; set; }
    }
}
