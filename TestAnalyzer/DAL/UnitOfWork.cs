﻿using System;
using DAL.Common;
using Model;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed = false;
        private readonly TestAnalyzerContext _context;
        private GenericRepository<TestRun> _testRunRepository;
        private GenericRepository<TestCycle> _testCycleRepository;
        private GenericRepository<ApplicationUser> _applicationUserRepository;
        private GenericRepository<TestMethod> _testMethodRepository;

        public UnitOfWork(TestAnalyzerContext context)
        {
            _context = context;
        }

        public IGenericRepository<TestRun> TestRunRepository
        {
            get
            {
                if (_testRunRepository == null)
                {
                    _testRunRepository = new GenericRepository<TestRun>(_context);
                }
                return _testRunRepository;
            }
        }

        public IGenericRepository<TestCycle> TestCycleRepository
        {
            get
            {
                if (_testCycleRepository == null)
                {
                    _testCycleRepository = new GenericRepository<TestCycle>(_context);
                }
                return _testCycleRepository;
            }
        }

        public IGenericRepository<ApplicationUser> ApplicationUserRepository
        {
            get
            {
                if (_applicationUserRepository == null)
                {
                    _applicationUserRepository = new GenericRepository<ApplicationUser>(_context);
                }
                return _applicationUserRepository;
            }
        }
        public IGenericRepository<TestMethod> TestMethodRepository
        {
            get
            {
                if (_testMethodRepository == null)
                {
                    _testMethodRepository = new GenericRepository<TestMethod>(_context);
                }
                return _testMethodRepository;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
