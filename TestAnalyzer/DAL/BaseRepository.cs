﻿namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Model;

    /// <summary>
    /// The base repository.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public class BaseRepository<T> : IBaseRepository<T> where T : Model.Base
    {
        /// <summary>
        /// Check that object is disposed.
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// The context.
        /// </summary>
        protected TestContext Context;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository{T}"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public BaseRepository(TestContext context)
        {
            this.Context = context;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Create new entity in DB.
        /// </summary>
        /// <param name="entity">
        /// Object to add to DB.
        /// </param>
        public void Create(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Entity cannot be null.");
            }

            this.Context.Set<T>().Add(entity);
        }

        /// <summary>
        /// Delete entity from DB.
        /// </summary>
        /// <param name="entity">Element to delete.</param>
        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Entity cannot be null.");
            }

            this.Context.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Get all elements.
        /// </summary>
        /// <returns>Return collection of objects.</returns>
        public IEnumerable<T> GetAll()
        {
            return this.Context.Set<T>();
        }

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Element to update.</param>
        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Entity cannot be null.");
            }

            this.Context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Save changes.
        /// </summary>
        public void Save()
        {
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.Context.Dispose();
                }
            }
            this.disposed = true;
        }
    }
}
