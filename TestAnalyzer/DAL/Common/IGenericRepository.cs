﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Common
{
    using System.Linq.Expressions;

    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        TEntity GetById(object id);

        TEntity Insert(TEntity entity);

        TEntity Delete(object id);

        TEntity Delete(TEntity entityToDelete);

        void Update(TEntity entityToUpdate);

        void Save();
    }
}
