﻿using System;
using Model;

namespace DAL.Common
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<TestRun> TestRunRepository { get; }
        IGenericRepository<TestCycle> TestCycleRepository { get; }
        IGenericRepository<TestMethod> TestMethodRepository { get; }
        IGenericRepository<ApplicationUser> ApplicationUserRepository { get; }
        void Save();
    }
}