﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Xml;
using DAL;
using DAL.Common;
using Microsoft.AspNet.Identity;
using Model;
using Service;

namespace WebAPI.Controllers
{
    using System.Linq;
    using System.Web.Http.Description;

    using Model.Enums;

    using Service.Helpers;
    using Service.TestSuits.NUnit;

    using WebGrease.Css.Extensions;

    [Authorize]
    public class TestRunController : ApiController
    {
        private readonly ITestCycleService _testCycleService;
        private readonly ITestRunService _testRunService;
        private readonly IParserManagerService _parserManagerService;
        private readonly IFileManagerService _fileRequestManagerService;

        public TestRunController(ITestCycleService testCycleService,
                                 ITestRunService testRunService,
                                 IParserManagerService parserManagerService,
                                 IFileManagerService fileRequestManagerService)
        {
            this._testCycleService = testCycleService;
            this._testRunService = testRunService;
            this._parserManagerService = parserManagerService;
            this._fileRequestManagerService = fileRequestManagerService;
        }

        //// GET api/TestRun
        [ResponseType(typeof(TestRun))]
        public HttpResponseMessage Get()
        {
            try
            {
                var testRuns = this._testRunService.GetTestRuns();
                return this.Request.CreateResponse(HttpStatusCode.OK, testRuns);
            }
            catch (Exception ex)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET api/TestRun/5
        [ResponseType(typeof(TestRun))]
        public HttpResponseMessage Get(int id)
        {
            var testRun = this._testRunService.GetById(id);
            if (testRun == null)
            {
                return this.Request.CreateResponse(HttpStatusCode.NotFound, $"Test cycle with id {id} doesn't exists");
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, testRun);
        }

        // GET api/TestRun/TestCycle/5
        [Route("api/TestRun/TestCycle/{id}")]
        [ResponseType(typeof(IEnumerable<TestRun>))]
        public HttpResponseMessage GetByTestCycleId(int id)
        {
            var testRuns = this._testRunService.GetByTestCycleId(id);
            if (testRuns == null)
            {
                return this.Request.CreateResponse(HttpStatusCode.NotFound, $"Test run with id {id} doesn't exists");
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, testRuns);
        }

        // TODO is Create method correct?
        // POST api/TestRun
        [ResponseType(typeof(TestRun))]
        public HttpResponseMessage Create([FromBody]TestRun testRun)
        {
            if (testRun == null)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TestCycle cannot be null");
            }

            try
            {
                this._testRunService.CreateAndSave(testRun);
                return this.Request.CreateResponse(HttpStatusCode.Created, testRun);
            }
            catch (Exception ex)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Couldnt save TestCycle, Error " + ex.Message);
            }
        }

        //PUT api/TestRun/5
        [ResponseType(typeof(TestRun))]
        public HttpResponseMessage Update(int id, [FromBody]TestRun testRun)
        {
            if (testRun == null || id < 1)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TestCycle cannot be null");
            }

            try
            {
                TestRun oldTestRun = this._testRunService.GetById(id);
                if (oldTestRun == null)
                {
                    return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TestCycle with given id does not exists");
                }

                oldTestRun = this._testRunService.Update(oldTestRun, testRun);

                return this.Request.CreateResponse(HttpStatusCode.Accepted, oldTestRun);
            }
            catch (Exception ex)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.NotModified, "Couldnt update TestCycle, Error " + ex.Message);
            }
        }

        // DELETE api/TestRun/id
        public HttpResponseMessage Delete(int id)
        {
            var testRun = this._testRunService.GetById(id);
            if (testRun == null)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Couldnt deleted TestCycle, TestCycle with given id does not existis ");
            }

            try
            {
                this._testRunService.Delete(testRun);
                return Request.CreateResponse(HttpStatusCode.OK, "TestCycle was removed");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotModified, "Couldnt update TestCycle, Error " + ex.Message);
            }
        }

        /// <summary>
        /// Handle uplaod xml. Request must have set Content-Type to Mulipart/Form-Data
        /// Inside request shold required: xmlFile as file and optionals testCycleId as int and comment as string
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("api/TestRun/Upload")]
        public async Task<HttpResponseMessage> PostXml()
        {
            if (!this.Request.Content.IsMimeMultipartContent())
            {
                return Request.CreateErrorResponse(HttpStatusCode.UnsupportedMediaType, "Invalid media type");
            }

            await _fileRequestManagerService.ReadFromRequest(this.Request);

            // TODO: if testRun isnt provided nor user dont choode default one. Should throw error
            int testCycleId = -1;

            var values = _fileRequestManagerService.ReadValues("testCycleId");

            if (values == null || !values.Any() || !int.TryParse(values[0], out testCycleId))
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Cant convert testCycleId to number");
            }

            string comment = string.Empty;
            values = _fileRequestManagerService.ReadValues("comment");
            if (values != null && values.Any())
            {
                comment = values[0];
            }

            if (!_fileRequestManagerService.ProviderContaintFileData())
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Xml file is missing");
            }

            var xmlDocument = _fileRequestManagerService.GetXmlDocument();

            if (xmlDocument == null)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Cannot load specified file.");
            }

            TestRun testRun = this._parserManagerService.TryParse(xmlDocument, comment, TestSuitsType.Unknow);

            if (testRun == null)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Couldn't parse xml to TestRun object");
            }

            TestCycle testCycle = _testCycleService.GetById(testCycleId);

            if (testCycle != null)
            {
                testCycle.TestsRuns.Add(testRun);
            }
            else
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, $"Test cycle with id = {testCycleId} does not exist.");
            }

            _testRunService.CreateAndSave(testRun);

            return this.Request.CreateResponse(HttpStatusCode.OK, new
            {
                comment = comment,
                testCycleId = testCycleId,
                conentOfXml = xmlDocument.OuterXml,
                userId = testRun.User.Id,
                testRunId = testRun.Id
            });
        }
    }
}
