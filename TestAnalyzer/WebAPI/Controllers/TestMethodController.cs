﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using Service;

namespace WebAPI.Controllers
{
    [Authorize]
    public class TestMethodController : ApiController
    {

        private readonly ITestMethodService _TestMethodService;

        public TestMethodController(ITestMethodService TestMethodService)
        {
            _TestMethodService = TestMethodService;
        }

        // GET api/TestMethod/GetByTestRunId/1
        [ActionName("GetByTestRunId")]
        [ResponseType(typeof(TestMethod))]
        public HttpResponseMessage GetByTestRunId(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _TestMethodService.GetTestMethodsByRunId(id));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET api/TestMethod/5
        [ResponseType(typeof(TestMethod))]
        public HttpResponseMessage Get(int id)
        {
            var TestMethod = _TestMethodService.GetById(id);
            if (TestMethod == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Test cycle with id {id} doesn't exists");
            }

            return Request.CreateResponse(HttpStatusCode.OK, TestMethod);
        }

        // POST api/TestMethod
        [ResponseType(typeof(TestMethod))]
        public HttpResponseMessage Add([FromBody]TestMethod TestMethod)
        {
            if (TestMethod == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TestMethod cannot be null");
            }

            try
            {
                _TestMethodService.CreateAndSave(TestMethod);
                return Request.CreateResponse(HttpStatusCode.Created, TestMethod);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Couldnt save TestMethod, Error " + ex.Message);
            }
        }

        // PUT api/TestMethod/5
        [ResponseType(typeof(TestMethod))]
        public HttpResponseMessage Put(int id, [FromBody]TestMethod TestMethod)
        {
            if (TestMethod == null || id < 1)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TestMethod cannot be null");
            }

            try
            {
                TestMethod oldTestMethod = _TestMethodService.GetById(id);
                if (oldTestMethod == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TestMethod with given id does not exists");
                }
                oldTestMethod = _TestMethodService.Update(oldTestMethod, TestMethod);

                return Request.CreateResponse(HttpStatusCode.Accepted, oldTestMethod);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotModified, "Couldnt update TestMethod, Error " + ex.Message);
            }
        }

        // DELETE api/<controller>/5
        public HttpResponseMessage Delete(int id)
        {
            var TestMethod = _TestMethodService.GetById(id);
            if (TestMethod == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Couldnt deleted TestMethod, TestMethod with given id does not existis ");
            }

            try
            {
                _TestMethodService.Delete(TestMethod);
                return Request.CreateResponse(HttpStatusCode.OK, "TestMethod was removed");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotModified, "Couldnt update TestMethod, Error " + ex.Message);
            }
        }

    }
}
