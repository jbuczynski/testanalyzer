﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Controllers
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Description;

    using Model;

    using Service.Helpers;

    //[Authorize]
    public class DbSeedController : ApiController
    {
        private TestAnalyzerContext context;

        public DbSeedController(TestAnalyzerContext context)
        {
            this.context = context;
        }

        [HttpGet]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage InitializeDb()
        {
            try
            {
                DbHelper.CleanUpDb(context);
                TestAnalyzerInitializerIfModelChanges.AddRecordToDb(context);
                context.SaveChanges();
                return this.Request.CreateResponse(HttpStatusCode.OK, "Seed successful.");
            }
            catch (Exception ex)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}