﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using Service;

namespace WebAPI.Controllers
{
    [Authorize]
    public class TestCycleController : ApiController
    {
        private readonly ITestCycleService _testCycleService;

        public TestCycleController(ITestCycleService testCycleService)
        {
            _testCycleService = testCycleService;
        }

        // GET api/TestCycle
        [ResponseType(typeof(TestCycle))]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _testCycleService.GetTestCycles());
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET api/TestCycle/5
        [ResponseType(typeof(TestCycle))]
        public HttpResponseMessage Get(int id)
        {
            var testCycle = _testCycleService.GetById(id);
            if (testCycle == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Test cycle with id {id} doesn't exists");
            }

            return Request.CreateResponse(HttpStatusCode.OK, testCycle);
        }

        // POST api/TestCycle
        [ResponseType(typeof(TestCycle))]
        public HttpResponseMessage Create([FromBody]TestCycle testCycle)
        {
            if (testCycle == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TestCycle cannot be null");
            }

            try
            {
                _testCycleService.CreateAndSave(testCycle);
                return Request.CreateResponse(HttpStatusCode.Created, testCycle);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Couldnt save TestCycle, Error " + ex.Message);
            }
        }

        // PUT api/TestCycle/5
        [ResponseType(typeof(TestCycle))]
        public HttpResponseMessage Update(int id, [FromBody]TestCycle testCycle)
        {
            if (testCycle == null || id < 1)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TestCycle cannot be null");
            }

            try
            {
                TestCycle oldTestCycle = _testCycleService.GetById(id);
                if (oldTestCycle == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "TestCycle with given id does not exists");
                }
                oldTestCycle = _testCycleService.Update(oldTestCycle, testCycle);

                return Request.CreateResponse(HttpStatusCode.Accepted, oldTestCycle);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotModified, "Couldnt update TestCycle, Error " + ex.Message);
            }
        }

        // DELETE api/<controller>/5
        public HttpResponseMessage Delete(int id)
        {
            var testCycle = _testCycleService.GetById(id);
            if (testCycle == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Couldnt deleted TestCycle, TestCycle with given id does not existis ");
            }

            try
            {
                _testCycleService.Delete(testCycle);
                return Request.CreateResponse(HttpStatusCode.OK, "TestCycle was removed");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotModified, "Couldnt update TestCycle, Error " + ex.Message);
            }
        }
    }
}