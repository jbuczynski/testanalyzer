﻿using DAL.Common;
using System.Data.Entity;
using Autofac;
using DAL;
using Model;

namespace WebAPI
{
    /// <summary>
    /// Use to register all class connected with DB.
    /// </summary>
    public class EFModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule());
            builder.RegisterType(typeof(TestAnalyzerContext)).As(typeof(DbContext)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(TestAnalyzerContext)).As(typeof(TestAnalyzerContext)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(UnitOfWork)).As(typeof(IUnitOfWork)).InstancePerRequest();
        }
    }
}