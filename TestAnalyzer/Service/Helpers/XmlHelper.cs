﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Helpers
{
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    using Service.TestSuits.NUnit;

    public class XmlHelper
    {
        public static T ReadResultTypeFromXml<T>(XmlDocument xml)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                T result;

                using (var stringReader = new StringReader(xml.OuterXml))
                using (var reader = XmlReader.Create(stringReader))
                {
                    result = (T)serializer.Deserialize(reader);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot deserialize xml document to object.", ex);
            }
        }

        public static XmlDocument LoadXmlDocument(string path)
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(path);
            return xmlDocument;
        }
    }
}
