﻿namespace Service.Helpers
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    using Model;

    public static class DbHelper
    {
        public static void ReseedIdentity<TEntity>(this DbContext context) where TEntity : class
        {
            string name = (context as IObjectContextAdapter).ObjectContext.CreateObjectSet<TEntity>().EntitySet.Name;

            var sqlCommand = $"dbcc checkident ('{name}', reseed, {0})";

            context.Database.ExecuteSqlCommand(sqlCommand);
        }

        public static void CleanUpDb(TestAnalyzerContext context)
        {
            context.Set<MethodParameter>().RemoveRange(context.MethodParameters);
            context.Set<OriginalInput>().RemoveRange(context.OriginalInputs);
            context.Set<TestCycle>().RemoveRange(context.TestCycles);
            context.Set<TestCycleConfiguration>().RemoveRange(context.TestCycleConfigurations);
            context.Set<TestMethod>().RemoveRange(context.TestMethods);
            context.Set<TestRun>().RemoveRange(context.TestRuns);
            context.Set<ApplicationUser>().RemoveRange(context.Users);

            context.ReseedIdentity<MethodParameter>();
            context.ReseedIdentity<OriginalInput>();
            context.ReseedIdentity<TestCycle>();
            context.ReseedIdentity<TestCycleConfiguration>();
            context.ReseedIdentity<TestMethod>();
            context.ReseedIdentity<TestRun>();

            context.SaveChanges();
        }
    }
}