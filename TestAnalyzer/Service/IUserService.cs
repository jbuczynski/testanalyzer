﻿using Model;

namespace Service
{
    public interface IUserService
    {
        string GetUserId();
        ApplicationUser GetUser();
    }
}
