﻿namespace Service.Parsers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Xml;

    using Model;
    using Model.Enums;

    using Service.Helpers;
    using Service.TestSuits.NUnit;

    public class NUnitXmlParserService : IXmlParser
    {
        #region IParser implementation

        public TestRun Parse(XmlDocument xml, string comment)
        {
            TestRun testRun = new TestRun();

            if (string.IsNullOrEmpty(xml?.OuterXml))
            {
                return null;
            }
            try
            {
                var resultType = XmlHelper.ReadResultTypeFromXml<resultType>(xml);
                testRun.Comment = comment;
                testRun.Name = resultType.name;
                testRun.TestsMethods = new List<TestMethod>();
                IEnumerable<testcaseType> testcaseTypes = this.GetAllTestSuits(resultType.testsuite);

                foreach (var testcaseType in testcaseTypes)
                {
                    TestMethod testMethod = new TestMethod
                    {
                        Result = this.GetTestResult(testcaseType),
                        ClassName = testcaseType.name,
                        Name = testcaseType.name,
                        Duration = this.GetDuration(testcaseType.time),
                        ErrorMsg = this.GetErrorMessage(testcaseType.Item),
                        TestRun = testRun
                    };

                    testRun.TestsMethods.Add(testMethod);
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return testRun;
        }

        #endregion

        #region Priate methods

        private IEnumerable<testcaseType> GetAllTestSuits(testsuiteType resultType)
        {
            List<testcaseType> testcaseTypes = new List<testcaseType>();
            this.GetTestSuite(resultType, testcaseTypes);
            return testcaseTypes;
        }

        private void GetTestSuite(testsuiteType resultType, List<testcaseType> testcaseTypes)
        {
            if (resultType.results?.Items != null)
            {
                foreach (var item in resultType.results.Items)
                {
                    var testsuiteType = item as testsuiteType;
                    if (testsuiteType != null)
                    {
                        this.GetTestSuite(testsuiteType, testcaseTypes);
                    }
                    else
                    {
                        var testCaseType = item as testcaseType;
                        if (testCaseType != null)
                        {
                            testcaseTypes.Add(testCaseType);
                        }
                    }
                }
            }
        }

        private double GetDuration(string time)
        {
            double executionTime;

            if (double.TryParse(time, NumberStyles.Any, new NumberFormatInfo(), out executionTime))
            {
                return executionTime;
            }

            return -1;
        }

        private Result GetTestResult(testcaseType testcaseType)
        {
            Result result;

            if (string.IsNullOrEmpty(testcaseType.result))
            {
                result = GetTestResultByBool(testcaseType);
            }
            else
            {
                result = GetTestResultByString(testcaseType);
            }

            return result;
        }

        private static Result GetTestResultByString(testcaseType testcaseType)
        {
            Result result;
            switch (testcaseType.result)
            {
                case "Error":
                    result = Result.Error;
                    break;
                case "Failure":
                    result = Result.Fail;
                    break;
                case "Success":
                    result = Result.Pass;
                    break;
                case "Inconclusive":
                    result = Result.Inconclusive;
                    break;
                case "Ignored":
                    result = Result.Ignored;
                    break;
                case "NotRunnable":
                    result = Result.NotRun;
                    break;
                default:
                    result = Result.Inconclusive; // default value of result.
                    break;
            }
            return result;
        }

        private static Result GetTestResultByBool(testcaseType testcaseType)
        {
            Result result;
            bool executed;
            if (bool.TryParse(testcaseType.executed, out executed))
            {
                if (executed)
                {
                    bool success;
                    if (bool.TryParse(testcaseType.success, out success))
                    {
                        result = success ? Result.Pass : Result.Fail;
                    }
                    else
                    {
                        result = Result.Inconclusive; // we don't know result.
                    }
                }
                else
                {
                    result = Result.NotRun;
                }
            }
            else
            {
                result = Result.Inconclusive;
            }
            return result;
        }

        private string GetErrorMessage(object item)
        {
            if (item == null)
            {
                return string.Empty;
            }

            switch (item.GetType().Name)
            {
                case "failureType":
                    {
                        var failureType = (TestSuits.NUnit.failureType)item;
                        return (failureType.message + Environment.NewLine + failureType.stacktrace).Trim();
                    }
                case "reasonType":
                    {
                        var reasonType = (TestSuits.NUnit.reasonType)item;
                        return reasonType.message?.Trim();
                    }
                default:
                    return string.Empty;
            }
        }

        #endregion
    }
}