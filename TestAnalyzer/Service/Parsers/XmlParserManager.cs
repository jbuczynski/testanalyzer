﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Parsers
{
    public class XmlParserManager : IXmlParserManager
    {
        private IXmlParser jUnitParser;

        private IXmlParser nUnitParser;

        public IXmlParser JUnitParser
        {
            get
            {
                if (jUnitParser == null)
                {
                    jUnitParser = new JUnitXmlParserService();
                }
                return jUnitParser;
            }
        }

        public IXmlParser NUnitParser
        {
            get
            {
                if (nUnitParser == null)
                {
                    nUnitParser = new NUnitXmlParserService();
                }
                return nUnitParser;
            }
        }
    }
}
