﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Parsers
{
    public interface IXmlParserManager
    {
        IXmlParser JUnitParser { get; }

        IXmlParser NUnitParser { get; }
    }
}
