﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Model;
using Service.TestSuits.NUnit;

namespace Service.Parsers
{
    using System.Xml;

    using Service.Helpers;

    public class JUnitXmlParserService : IXmlParser
    {
        public TestRun Parse(XmlDocument xml, string comment)
        {
            try
            {
                testsuite resultingTestSuit = XmlHelper.ReadResultTypeFromXml<testsuite>(xml);

                TestRun testRun = new TestRun
                {
                    Comment = comment,
                    Name = resultingTestSuit.name,
                    TestsMethods = new List<TestMethod>()
                };

                foreach (var testCase in resultingTestSuit.testcase)
                {
                    TestMethod testMethod = new TestMethod
                    {
                        ClassName = testCase.classname,
                        Name = testCase.name
                    };

                    double duration;
                    if (double.TryParse(testCase.time, out duration))
                    {
                        testMethod.Duration = duration;
                    }
                    else
                    {
                        testMethod.Duration = -1;
                    }

                    if (testCase.error != null && testCase.error.Any())
                    {
                        if (!string.IsNullOrEmpty(testCase.error[0].Value))
                        {
                            testMethod.ErrorMsg = testCase.error[0].Value;
                            testMethod.Result = Model.Enums.Result.Error;
                        }
                    }
                    else if (testCase.failure != null && testCase.failure.Any())
                    {
                        if (!string.IsNullOrEmpty(testCase.failure[0].Value))
                        {
                            testMethod.ErrorMsg = testCase.failure[0].Value;
                            testMethod.Result = Model.Enums.Result.Fail;
                        }
                    }
                    else
                    {
                        testMethod.Result = Model.Enums.Result.Pass;
                    }

                    testRun.TestsMethods.Add(testMethod);
                }

                return testRun;
            }
            catch
            {
                return null;
            }

        }
    }
}
