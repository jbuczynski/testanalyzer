﻿namespace Service.Common
{
    using System.Collections.Generic;

    using Model;

    public interface IEntityService<T> : IService
        where T : Base
    {
        void Create(T entity);
        void Delete(T entity);
        IEnumerable<T> GetAll();
        void Update(T entity);
    }
}
