﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Service
{
    using System.Xml;

    using Model.Enums;

    public interface IParserManagerService
    {
        TestRun TryParse(XmlDocument xml, string comment, TestSuitsType testSuitsName);
    }
}
