﻿using System.Collections.Generic;
using Model;


namespace Service
{
    public interface ITestMethodService
    {
        TestMethod GetById(int id);
        IEnumerable<TestMethod> GetTestMethodsByRunId(int id);
        TestMethod CreateAndSave(TestMethod testMethod);
        TestMethod Update(TestMethod oldTestMethod, TestMethod testMethod);
        void Delete(TestMethod testMethod);
    }
}