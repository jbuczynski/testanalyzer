﻿using Model;

namespace Service
{
    using System.Collections.Generic;

    public interface ITestRunService
    {
        TestRun GetById(int id);

        IEnumerable<TestRun> GetByTestCycleId(int id);
        IEnumerable<TestRun> GetTestRuns();

        TestRun CreateAndSave(TestRun testRun);

        TestRun Update(TestRun oldTestRun, TestRun testRun);

        void Delete(TestRun testRun);
    }
}
