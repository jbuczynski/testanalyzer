﻿using System.Linq;
using DAL.Common;
using Model;

namespace Service
{
    using System;
    using System.Collections.Generic;

    public class TestRunService : ITestRunService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;

        public TestRunService(IUnitOfWork unitOfWork, IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        public TestRun GetById(int id)
        {
            var userId = this._userService.GetUserId();
            var testRun = this._unitOfWork.TestRunRepository.Get(tr => tr.User.Id == userId && tr.Id == id).FirstOrDefault();
            return testRun;
        }

        public IEnumerable<TestRun> GetTestRuns()
        {
            var userId = this._userService.GetUserId();
            var testRuns = this._unitOfWork.TestRunRepository.Get(testRun => testRun.User.Id == userId);
            return testRuns;
        }

        public TestRun CreateAndSave(TestRun testRun)
        {
            testRun.User = this._userService.GetUser();
            this._unitOfWork.TestRunRepository.Insert(testRun);
            this._unitOfWork.TestRunRepository.Save();
            return testRun;
        }

        public TestRun Update(TestRun oldTestRun, TestRun testRun)
        {
            oldTestRun.UploadType = testRun.UploadType;
            oldTestRun.Comment = testRun.Comment;
            oldTestRun.Name = testRun.Name;
            oldTestRun.TestCycleId = testRun.TestCycleId;
            
            this._unitOfWork.TestRunRepository.Update(oldTestRun);
            this._unitOfWork.TestRunRepository.Save();
            return oldTestRun;
        }

        public void Delete(TestRun testRun)
        {
            this._unitOfWork.TestRunRepository.Delete(testRun);
            this._unitOfWork.TestRunRepository.Save();
        }

        public IEnumerable<TestRun> GetByTestCycleId(int id)
        {
            var userId = this._userService.GetUserId();
            var testRuns = this._unitOfWork.TestRunRepository.Get(testRun => testRun.User.Id == userId && testRun.TestCycleId == id);
            return testRuns;
        }
    }
}
