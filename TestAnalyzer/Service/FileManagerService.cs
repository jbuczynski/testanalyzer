﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    using System.Net;
    using System.Net.Http;
    using System.Web;
    using System.Xml;

    using Service.Helpers;

    public class FileManagerService : IFileManagerService
    {
        private string _workingDirectory;
        private TestRunMultipartFormDataStreamProvider _provider;

        public FileManagerService()
        {
            _workingDirectory = HttpContext.Current.Server.MapPath("~/App_Data");
            _provider = new TestRunMultipartFormDataStreamProvider(_workingDirectory);
        }

        public string GetWorkingDirectoryPath => this._workingDirectory;

        public async Task<TestRunMultipartFormDataStreamProvider> ReadFromRequest(HttpRequestMessage request)
        {
            if (request != null)
            {
                await request.Content.ReadAsMultipartAsync(_provider);
            }

            return null;
        }

        public string[] ReadValues(string name)
        {
            var result = _provider.FormData.GetValues(name);
            return result;
        }

        public bool ProviderContaintFileData()
        {
            var result = _provider.FileData.Count > 0;
            return result;
        }

        public string GetLocalFileName()
        {
            var fileName = _provider.FileData[0].LocalFileName;
            return fileName;
        }

        public XmlDocument GetXmlDocument()
        {
            try
            {
                var path = this.GetLocalFileName();
                var document = XmlHelper.LoadXmlDocument(path);
                return document;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
