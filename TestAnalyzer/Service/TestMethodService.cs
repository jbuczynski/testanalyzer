﻿using System.Collections.Generic;
using System.Linq;
using DAL.Common;
using Model;

namespace Service
{
    public class TestMethodService : ITestMethodService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;

        public TestMethodService(IUnitOfWork unitOfWork, IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        public TestMethod GetById(int id)
        {
            return _unitOfWork.TestMethodRepository.Get(testMethod => testMethod.Id == id).FirstOrDefault();
        }

        public IEnumerable<TestMethod> GetTestMethodsByRunId(int id)
        {
            return _unitOfWork.TestMethodRepository.Get(testMethod => testMethod.TestRunId == id);
        }

        public TestMethod CreateAndSave(TestMethod testMethod)
        {
            _unitOfWork.TestMethodRepository.Insert(testMethod);
            _unitOfWork.TestMethodRepository.Save();
            return testMethod;
        }

        public TestMethod Update(TestMethod oldTestMethod, TestMethod testMethod)
        {
            oldTestMethod.Name = testMethod.Name;
            oldTestMethod.Result = testMethod.Result;
            oldTestMethod.ClassName = testMethod.ClassName;
            oldTestMethod.ErrorMsg = testMethod.ErrorMsg;
            oldTestMethod.Duration = testMethod.Duration;
            oldTestMethod.TestRunId = testMethod.TestRunId;

            _unitOfWork.TestMethodRepository.Update(oldTestMethod);
            _unitOfWork.TestMethodRepository.Save();
            return oldTestMethod;
        }

        public void Delete(TestMethod testMethod)
        {
            _unitOfWork.TestMethodRepository.Delete(testMethod);
            _unitOfWork.TestMethodRepository.Save();
        }

    }
}