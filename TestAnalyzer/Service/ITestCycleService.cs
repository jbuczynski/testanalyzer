﻿using System.Collections.Generic;
using Model;

namespace Service
{
    public interface ITestCycleService
    {
        TestCycle GetById(int id);
        IEnumerable<TestCycle> GetTestCycles();
        TestCycle CreateAndSave(TestCycle testCycle);
        TestCycle Update(TestCycle oldTestCycle, TestCycle testCycle);
        void Delete(TestCycle testCycle);
    }
}
