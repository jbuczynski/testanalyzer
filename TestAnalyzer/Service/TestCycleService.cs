﻿using System.Collections.Generic;
using System.Linq;
using DAL.Common;
using Model;

namespace Service
{
    public class TestCycleService : ITestCycleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;

        public TestCycleService(IUnitOfWork unitOfWork, IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        public TestCycle GetById(int id)
        {
            var userId = _userService.GetUserId();
            return _unitOfWork.TestCycleRepository.Get(testCycle => testCycle.User.Id == userId && testCycle.Id == id).FirstOrDefault();
        }

        public IEnumerable<TestCycle> GetTestCycles()
        {
            var userId = _userService.GetUserId();
            return _unitOfWork.TestCycleRepository.Get(testCycle => testCycle.User.Id == userId);
        }

        public TestCycle CreateAndSave(TestCycle testCycle)
        {
            testCycle.User = _userService.GetUser();
            _unitOfWork.TestCycleRepository.Insert(testCycle);
            _unitOfWork.TestCycleRepository.Save();
            return testCycle;
        }

        public TestCycle Update(TestCycle oldTestCycle, TestCycle testCycle)
        {
            oldTestCycle.Comment = testCycle.Comment;
            oldTestCycle.Name = testCycle.Name;
            oldTestCycle.IsApproved = testCycle.IsApproved;

            _unitOfWork.TestCycleRepository.Update(oldTestCycle);
            _unitOfWork.TestCycleRepository.Save();
            return oldTestCycle;
        }

        public void Delete(TestCycle testCycle)
        {
            _unitOfWork.TestCycleRepository.Delete(testCycle);
            _unitOfWork.TestCycleRepository.Save();
        }
    }
}
