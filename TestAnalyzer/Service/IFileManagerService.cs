﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    using System.Net.Http;
    using System.Xml;

    using Service.Helpers;

    public interface IFileManagerService
    {
        string GetWorkingDirectoryPath { get; }

        Task<TestRunMultipartFormDataStreamProvider> ReadFromRequest(HttpRequestMessage request);

        string[] ReadValues(string name);

        bool ProviderContaintFileData();

        string GetLocalFileName();

        XmlDocument GetXmlDocument();
    }
}
