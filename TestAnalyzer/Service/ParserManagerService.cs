﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Model;
using Model.Enums;
using Service.Parsers;

namespace Service
{
    using System.Xml;

    public class ParserManagerService : IParserManagerService
    {
        private readonly IXmlParserManager _parsingManager;

        public ParserManagerService(IXmlParserManager parsingManager)
        {
            _parsingManager = parsingManager;
        }

        public TestRun TryParse(XmlDocument xml, string comment = null, TestSuitsType testSuitsName = TestSuitsType.Unknow)
        {
            try
            {
                TestRun testRun = null;

                if (testSuitsName != TestSuitsType.Unknow)
                {
                    testRun = this.TryParseByType(xml, comment, testSuitsName);
                }

                if (testRun == null)
                {
                    testRun = this.TryParseUnknowType(xml, comment);
                }

                return testRun;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private TestRun TryParseUnknowType(XmlDocument xml, string comment)
        {
            var testRun = this._parsingManager.JUnitParser.Parse(xml, comment);

            if (testRun != null)
            {
                return testRun;
            }

            testRun = this._parsingManager.NUnitParser.Parse(xml, comment);

            if (testRun != null)
            {
                return testRun;
            }

            throw new ArgumentException("Couldn't parse xml to testRun by any parser.", nameof(xml));
        }

        private TestRun TryParseByType(XmlDocument xml, string comment, TestSuitsType testSuitsName)
        {
            switch (testSuitsName)
            {
                case TestSuitsType.JUnit:
                    var jUnitResult = _parsingManager.JUnitParser.Parse(xml, comment);
                    return jUnitResult;
                case TestSuitsType.NUnit:
                    var nUnitResult = _parsingManager.NUnitParser.Parse(xml, comment);
                    return nUnitResult;
                default:
                    throw new ArgumentException(
                        $"Cannot found TestSuitsType named {testSuitsName}.", nameof(testSuitsName));
            }
        }
    }
}
