﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests.Repositories
{
    using System.Data.Entity;

    using DAL;
    using DAL.Common;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model;

    using Moq;

    [TestClass]
    public class UnitOfWorkTest
    {
        [TestMethod]
        public void UnitOfWorkImplementIUnitOfWorkInterface()
        {
            var customDbContextMock = new Mock<TestAnalyzerContext>("TestConncection");

            var unitOfWork = new UnitOfWork(customDbContextMock.Object);

            Assert.IsNotNull(unitOfWork);
            Assert.IsInstanceOfType(unitOfWork, typeof(IUnitOfWork), $"{nameof(UnitOfWork)} is instance of {nameof(IUnitOfWork)}");
        }

        [TestMethod]
        public void MethodSaveWasCalledCorrectly()
        {
            var customDbContextMock = new Mock<TestAnalyzerContext>("TestConncection");
            customDbContextMock
                .Setup(x => x.SaveChanges())
                .Returns(1);

            var unitOfWork = new UnitOfWork(customDbContextMock.Object);

            unitOfWork.Save();

            customDbContextMock.Verify(c => c.SaveChanges());
        }

        [TestMethod]
        public void CanGetTestRunRepositoryWithNoInitField()
        {
            var customDbContextMock = new Mock<TestAnalyzerContext>("TestConncection");

            var unitOfWork = new UnitOfWork(customDbContextMock.Object);

            var repo = unitOfWork.TestRunRepository;

            Assert.IsNotNull(repo);
            Assert.IsInstanceOfType(repo, typeof(IGenericRepository<TestRun>));
        }

        [TestMethod]
        public void CanGetTestCycleRepository()
        {
            var customDbContextMock = new Mock<TestAnalyzerContext>("TestConncection");

            var unitOfWork = new UnitOfWork(customDbContextMock.Object);

            var repo = unitOfWork.TestCycleRepository;

            Assert.IsNotNull(repo);
            Assert.IsInstanceOfType(repo, typeof(IGenericRepository<TestCycle>));
        }

        [TestMethod]
        public void CanGetApplicationUserRepository()
        {
            var customDbContextMock = new Mock<TestAnalyzerContext>("TestConncection");

            var unitOfWork = new UnitOfWork(customDbContextMock.Object);

            var repo = unitOfWork.ApplicationUserRepository;

            Assert.IsNotNull(repo);
            Assert.IsInstanceOfType(repo, typeof(IGenericRepository<ApplicationUser>));
        }

        [TestMethod]
        public void CanCallMethodDispose()
        {
            var customDbContextMock = new Mock<TestAnalyzerContext>("TestConncection");

            var unitOfWork = new UnitOfWork(customDbContextMock.Object);

            unitOfWork.Dispose();
        }
    }
}
