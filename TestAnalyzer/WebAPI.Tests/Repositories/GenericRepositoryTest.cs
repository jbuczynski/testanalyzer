﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#pragma warning disable 618

namespace WebAPI.Tests.Repositories
{
    using System.Collections;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.IO;

    using DAL;
    using DAL.Common;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model;
    using Model.Enums;
    using WebAPI.Tests;
    using Moq;

    using Service.Helpers;

    [TestClass]
    public class GenericRepositoryTest
    {
        private GenericRepository<TestRun> testRunRepo;

        private TestAnalyzerContext context;

        private ApplicationUser[] users;

        private TestCycle[] testCycles;

        private TestRun[] testRuns;

        #region Test init and clean up

        /// <summary>
        /// The set up.
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Empty));
            this.context = new TestAnalyzerContext("TestConnection");

            this.testRunRepo = new GenericRepository<TestRun>(this.context);

            TryInitDb();
        }

        private void TryInitDb()
        {
            bool seedCompleted = false;
            var maxRetryCount = 3;
            var retry = 0;

            while (!seedCompleted && retry < maxRetryCount)
            {
                try
                {
                    this.Init();
                    seedCompleted = true;
                }
                catch (DbEntityValidationException)
                // previous test session finished with exception and didn't clean up your test database.
                {
                    using (var context = new TestAnalyzerContext("TestConnection"))
                    {
                        DbHelper.CleanUpDb(context);
                    }
                }
                retry += 1;
            }
        }

        /// <summary>
        /// The test finalize.
        /// </summary>
        [TestCleanup]
        public void TestFinalize()
        {
            DbHelper.CleanUpDb(context);
            this.context.Dispose();
        }

        #endregion

        [TestMethod]
        public void GetByIdReturnExpectedTestRun()
        {
            int expectedId = 1;
            var expected = this.testRuns[0];
            var result = this.testRunRepo.GetById(expectedId);

            Assert.IsNotNull(result);
            Assert.AreEqual(expectedId, result.Id);
            Assert.AreEqual(expected.Name, result.Name);
            Assert.AreEqual(expected.UploadType, result.UploadType);
            Assert.AreEqual(expected.Comment, result.Comment);
            Assert.AreEqual(expected.TestCycleId, result.TestCycleId);
            Assert.IsTrue(result.Added < DateTime.Now);
        }

        /// <summary>
        /// The insert puts element into db set.
        /// </summary>
        [TestMethod]
        public void InsertPutsElementIntoDbReturnUpdatedEntity()
        {
            var testCycle = new GenericRepository<TestCycle>(this.context).GetById(1); // we need this value from db, because EF tracking entities.
            var user = new GenericRepository<ApplicationUser>(this.context).GetById("user1");

            TestRun newTestRun = new TestRun()
            {
                Id = 4,
                TestCycle = testCycle,
                Comment = "New TestRun Comment",
                Name = "New TestRun Name",
                UploadType = UploadType.Ci,
                User = user,
            };

            var result = this.testRunRepo.Insert(newTestRun);
            this.testRunRepo.Save();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Id > 0);
            Assert.AreEqual(newTestRun.Name, result.Name);
            Assert.AreEqual(newTestRun.UploadType, result.UploadType);
            Assert.AreEqual(newTestRun.Comment, result.Comment);
            Assert.AreEqual(newTestRun.TestCycleId, result.TestCycleId);
            Assert.AreEqual(newTestRun.TestCycle, result.TestCycle);
            Assert.AreEqual(newTestRun.User, result.User);
            Assert.IsNotNull(result.Added);
            Assert.IsNotNull(result.Modified);
            Assert.IsTrue(result.Added < DateTime.Now);
            Assert.IsTrue(result.Added == result.Modified);
        }

        [TestMethod]
        public void DeleteWithIdReturnElementNotLongerExits()
        {
            var itemToDelete = this.testRunRepo.GetById(3);
            var result = this.testRunRepo.Delete(3);
            this.testRunRepo.Save();

            var itemAfterDelete = this.testRunRepo.GetById(3);
            Assert.IsNotNull(result);
            Assert.IsNull(itemAfterDelete, "Item after delete and save changes no longer exits in db.");
        }

        [TestMethod]
        public void DeleteElementWhichIsDetached()
        {
            var itemToDelete = this.testRunRepo.GetById(2);

            this.context.Entry(itemToDelete).State = EntityState.Detached;
            var result = this.testRunRepo.Delete(itemToDelete);

            this.testRunRepo.Save();

            var itemAfterDelete = this.testRunRepo.GetById(itemToDelete.Id);
            Assert.IsNotNull(result);
            Assert.IsNull(itemAfterDelete, "Item after delete and save changes no longer exits in db.");
        }

        [TestMethod]
        public void UpdateModifyEntries()
        {
            var itemToUpdate = this.testRunRepo.GetById(1);
            itemToUpdate.Comment = "Updated comment by unit test.";

            this.testRunRepo.Update(itemToUpdate);

            var stateBeforeSaveChanges = this.context.Entry(itemToUpdate).State;
            this.testRunRepo.Save();

            var afterUpdate = this.testRunRepo.GetById(itemToUpdate.Id);

            Assert.AreEqual(EntityState.Modified, stateBeforeSaveChanges);
            Assert.IsNotNull(afterUpdate);
            Assert.AreEqual(afterUpdate.Id, itemToUpdate.Id);
            Assert.AreEqual(itemToUpdate.Name, afterUpdate.Name);
            Assert.AreEqual(itemToUpdate.UploadType, afterUpdate.UploadType);
            Assert.AreEqual(itemToUpdate.Comment, afterUpdate.Comment);
            Assert.AreEqual(itemToUpdate.TestCycleId, afterUpdate.TestCycleId);
            Assert.AreEqual(itemToUpdate.TestCycle, afterUpdate.TestCycle);
            Assert.AreEqual(itemToUpdate.User, afterUpdate.User);
            Assert.IsNotNull(itemToUpdate.Added);
            Assert.IsNotNull(itemToUpdate.Modified);
            Assert.IsTrue(itemToUpdate.Added < DateTime.Now);
            Assert.IsTrue(itemToUpdate.Added <= afterUpdate.Modified);
        }

        [TestMethod]
        public void GetWitoutArgumentsReturnsAllEntries()
        {
            var allTestRuns = this.testRuns.ToList();
            var allEntries = this.testRunRepo.Get();

            Assert.IsNotNull(allEntries);
            Assert.AreEqual(allTestRuns.Count, allEntries.Count());
            foreach (var testRun in allTestRuns)
            {
                var dbTestRun = allEntries.FirstOrDefault(c => c.Id == testRun.Id);

                Assert.IsNotNull(dbTestRun);
                Assert.AreEqual(testRun.Id, dbTestRun.Id);
                Assert.AreEqual(testRun.Name, dbTestRun.Name);
                Assert.AreEqual(testRun.UploadType, dbTestRun.UploadType);
                Assert.AreEqual(testRun.Comment, dbTestRun.Comment);
                Assert.AreEqual(testRun.TestCycleId, dbTestRun.TestCycleId);
                Assert.IsNotNull(dbTestRun.Added);
                Assert.IsNotNull(dbTestRun.Modified);
                Assert.IsTrue(testRun.Added < DateTime.Now);
            }
        }

        [TestMethod]
        public void GetWithUserIdAndIncludeUser()
        {
            string userId = "user1";
            var allTestRuns = this.testRuns.Where(c => c.User.Id == userId).ToList();
            var allEntries = this.testRunRepo.Get(tr => tr.User.Id == userId, null, "User");

            Assert.IsNotNull(allEntries);
            Assert.AreEqual(allTestRuns.Count, allEntries.Count());
            foreach (var testRun in allTestRuns)
            {
                var dbTestRun = allEntries.FirstOrDefault(c => c.Id == testRun.Id);

                Assert.IsNotNull(dbTestRun);
                Assert.IsNotNull(dbTestRun.User);
                Assert.AreEqual(testRun.User.Id, dbTestRun.User.Id);
                Assert.AreEqual(testRun.User.Email, dbTestRun.User.Email);
                Assert.AreEqual(testRun.User.UserName, dbTestRun.User.UserName);
                Assert.AreEqual(testRun.User, dbTestRun.User);
                Assert.AreEqual(testRun.Id, dbTestRun.Id);
                Assert.AreEqual(testRun.Name, dbTestRun.Name);
                Assert.AreEqual(testRun.UploadType, dbTestRun.UploadType);
                Assert.AreEqual(testRun.Comment, dbTestRun.Comment);
                Assert.AreEqual(testRun.TestCycleId, dbTestRun.TestCycleId);
                Assert.IsNotNull(dbTestRun.Added);
                Assert.IsNotNull(dbTestRun.Modified);
                Assert.IsTrue(testRun.Added < DateTime.Now);
            }
        }

        [TestMethod]
        public void GetTestRunIncludeAllDependencies()
        {
            var allEntries = this.testRunRepo.Get(includeProperties: "User,TestCycle,TestsMethods");

            Assert.IsNotNull(allEntries);
            Assert.AreEqual(this.testRuns.Count(), allEntries.Count());
            foreach (var testRun in this.testRuns)
            {
                var dbTestRun = allEntries.FirstOrDefault(c => c.Id == testRun.Id);

                Assert.IsNotNull(dbTestRun);
                Assert.IsNotNull(dbTestRun);
                Assert.IsNotNull(dbTestRun.User);
                Assert.IsNotNull(dbTestRun.TestsMethods);
            }
        }

        [TestMethod]
        public void GetTestRunWithOrderByIds()
        {
            var allEntries = this.testRunRepo.Get(orderBy: runs => runs.OrderByDescending(c => c.Id)).ToArray();

            Assert.IsNotNull(allEntries);
            Assert.AreEqual(this.testRuns.Count(), allEntries.Count());

            Assert.AreEqual(3, allEntries[0].Id);
            Assert.AreEqual(2, allEntries[1].Id);
            Assert.AreEqual(1, allEntries[2].Id);
        }

        #region Helpers

        /// <summary>
        /// The init.
        /// </summary>
        private void Init()
        {
            this.users = CreateUsers();

            this.testCycles = GetTestCycles(this.users);

            this.testRuns = GetTestRuns(this.users, this.testCycles);

            this.context.SaveChanges();
        }

        private TestRun[] GetTestRuns(ApplicationUser[] users, TestCycle[] testCycles)
        {
            TestRun[] testRuns = new TestRun[3];
            TestRun testRun1 = new TestRun
            {
                Id = 1,
                UploadType = UploadType.Ci,
                Comment = "Tesr run comment 1",
                User = users[3],
                TestCycle = testCycles[0],
                TestCycleId = testCycles[0].Id, // normally ef fill this property, but for test we need fill it manually.
            };
            this.context.TestRuns.Add(testRun1);

            TestRun testRun2 = new TestRun
            {
                Id = 2,
                UploadType = UploadType.Ci,
                Comment = "Tesr run comment 2",
                User = users[3],
                TestCycle = testCycles[1],
                TestCycleId = testCycles[1].Id, // normally ef fill this property, but for test we need fill it manually.
            };
            this.context.TestRuns.Add(testRun2);

            TestRun testRun3 = new TestRun
            {
                Id = 3,
                UploadType = UploadType.User,
                Comment = "Tesr run comment 3",
                User = users[1],
                TestCycle = testCycles[2],
                TestCycleId = testCycles[2].Id, // normally ef fill this property, but for test we need fill it manually.
            };
            this.context.TestRuns.Add(testRun3);

            testRuns[0] = testRun1;
            testRuns[1] = testRun2;
            testRuns[2] = testRun3;

            return testRuns;
        }

        private TestCycle[] GetTestCycles(ApplicationUser[] users)
        {
            var testCycles = new TestCycle[3];
            TestCycle testCycle1 = new TestCycle()
            {
                Id = 1,
                Comment = "TestCycle 1",
                IsApproved = true,
                Name = "TestCycle 1",
                User = users[0]
            };
            context.TestCycles.AddOrUpdate(testCycle1);

            TestCycle testCycle2 = new TestCycle()
            {
                Id = 2,
                Comment = "TestCycle 2",
                IsApproved = true,
                Name = "TestCycle 2",
                User = users[1]
            };
            context.TestCycles.AddOrUpdate(testCycle2);

            TestCycle testCycle3 = new TestCycle()
            {
                Id = 3,
                Comment = "TestCycle 3",
                IsApproved = true,
                Name = "TestCycle 3",
                User = users[2]
            };
            context.TestCycles.AddOrUpdate(testCycle3);

            testCycles[0] = testCycle1;
            testCycles[1] = testCycle2;
            testCycles[2] = testCycle3;

            return testCycles;
        }

        /// <summary>
        /// The create users.
        /// </summary>
        /// <returns>
        /// The <see cref="ApplicationUser[]"/>.
        /// </returns>
        private ApplicationUser[] CreateUsers()
        {
            UserManager<ApplicationUser> manager = null;
            if (context != null)
                manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            ApplicationUser[] users = new ApplicationUser[4];
            ApplicationUser user1 = new ApplicationUser
            {
                Id = "user1",
                Email = "user1@email.com",
                UserName = "user1@email.com",
                EmailConfirmed = true
            };
            manager.Create(user1, "zaq1@WSX");

            var user2 = new ApplicationUser
            {
                Id = "user2",
                Email = "user2@email.com",
                UserName = "user2@email.com",
                EmailConfirmed = true
            };
            manager.Create(user2, "zaq1@WSX");

            var user3 = new ApplicationUser
            {
                Id = "user3",
                Email = "user3@email.com",
                UserName = "user3@email.com",
                EmailConfirmed = true
            };
            manager.Create(user3, "zaq1@WSX");

            var user4 = new ApplicationUser
            {
                Id = "user4",
                Email = "user4@email.com",
                UserName = "user4@email.com",
                EmailConfirmed = true
            };
            manager.Create(user4, "zaq1@WSX");

            users[0] = user1;
            users[1] = user2;
            users[2] = user3;
            users[3] = user4;

            return users;
        }
    }

    #endregion
}
