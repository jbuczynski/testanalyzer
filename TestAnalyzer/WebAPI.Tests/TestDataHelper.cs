﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests
{
    using System.Xml;

    using Model;
    using Model.Enums;

    public class TestDataHelper
    {
        #region Fields

        public const string NUnitPath = "Data/NUnit_testResult.xml";
        private const string JUnitPath = "Data/nose2.xml";
        private const string InvalidXml = @"<?xml version=""1.0"" encoding=""utf - 8"" standalone=""no""?><test-resulted></test-resulted>";

        private static XmlDocument nUnitDocument;
        private static XmlDocument jUnitDocument;
        private static XmlDocument invalidDocument;

        #endregion

        #region Properties

        public static XmlDocument NUnitDocument
        {
            get
            {
                if (nUnitDocument == null)
                {
                    nUnitDocument = new XmlDocument();
                    nUnitDocument.Load(NUnitPath);
                }

                return nUnitDocument;
            }
        }

        public static XmlDocument JUnitDocument
        {
            get
            {
                if (jUnitDocument == null)
                {
                    jUnitDocument = new XmlDocument();
                    jUnitDocument.Load(JUnitPath);
                }

                return jUnitDocument;
            }
        }

        public static XmlDocument InvalidXmlDocument
        {
            get
            {
                if (invalidDocument == null)
                {
                    invalidDocument = new XmlDocument();
                    invalidDocument.LoadXml(InvalidXml);
                }

                return invalidDocument;
            }
        }

        #endregion

        #region Fake test run - based on NUnit_testResult.xml

        public static TestRun GetTestRunBasedOnNUnit_testResult()
        {
            var testRun = new TestRun();
            testRun.Comment = "Test Run Comment";
            testRun.Name = @"/home/charlie/Dev/NUnit/nunit-2.5/work/src/bin/Debug/tests/mock-assembly.dll";
            testRun.TestsMethods = new List<TestMethod>()
            {
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.FailingTest",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.FailingTest",
                        Result = Result.Fail,
                        Duration = 0.013,
                        ErrorMsg =
                            $@"Intentional failure{Environment.NewLine}at NUnit.Tests.Assemblies.MockTestFixture.FailingTest () [0x00000] in /home/charlie/Dev/NUnit/nunit-2.5/work/src/tests/mock-assembly/MockAssembly.cs:121",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.InconclusiveTest",
                        Result = Result.Inconclusive,
                        Duration = 0.001,
                        ErrorMsg ="No valid data"
                    },

                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.MockTest1",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.MockTest1",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.MockTest2",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.MockTest2",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.MockTest3",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.MockTest3",
                        Result = Result.Pass,
                        Duration = 0.001,
                        ErrorMsg = "Succeeded!"
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.MockTest4",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.MockTest4",
                        Result = Result.Ignored,
                        Duration = -1,
                        ErrorMsg = "ignoring this test method for now"
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.MockTest5",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.MockTest5",
                        Result = Result.NotRun,
                        Duration = -1,
                        ErrorMsg = "Method is not public"
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.NotRunnableTest",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.NotRunnableTest",
                        Result = Result.NotRun,
                        Duration = -1,
                        ErrorMsg = "No arguments were provided"
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.TestWithException",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.TestWithException",
                        Result = Result.Error,
                        Duration = 0.001,
                        ErrorMsg = $@"System.ApplicationException : Intentional Exception{Environment.NewLine}at NUnit.Tests.Assemblies.MockTestFixture.TestWithException () [0x00000] in /home/charlie/Dev/NUnit/nunit-2.5/work/src/tests/mock-assembly/MockAssembly.cs:153"
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Assemblies.MockTestFixture.TestWithManyProperties",
                        Name = "NUnit.Tests.Assemblies.MockTestFixture.TestWithManyProperties",
                        Result = Result.Pass,
                        Duration = 0.0,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.BadFixture.SomeTest",
                        Name = "NUnit.Tests.BadFixture.SomeTest",
                        Result = Result.NotRun,
                        Duration = -1,
                        ErrorMsg = "No suitable constructor was found"
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.FixtureWithTestCases.GenericMethod<Double>(9.2d,11.7d)",
                        Name = "NUnit.Tests.FixtureWithTestCases.GenericMethod<Double>(9.2d,11.7d)",
                        Result = Result.Pass,
                        Duration = 0.007,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.FixtureWithTestCases.GenericMethod<Int32>(2,4)",
                        Name = "NUnit.Tests.FixtureWithTestCases.GenericMethod<Int32>(2,4)",
                        Result = Result.Pass,
                        Duration = 0.001,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.FixtureWithTestCases.MethodWithParameters(9,11)",
                        Name = "NUnit.Tests.FixtureWithTestCases.MethodWithParameters(9,11)",
                        Result = Result.Pass,
                        Duration = 0.003,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.FixtureWithTestCases.MethodWithParameters(2,2)",
                        Name = "NUnit.Tests.FixtureWithTestCases.MethodWithParameters(2,2)",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.GenericFixture<Double>(11.5d).Test1",
                        Name = "NUnit.Tests.GenericFixture<Double>(11.5d).Test1",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.GenericFixture<Double>(11.5d).Test2",
                        Name = "NUnit.Tests.GenericFixture<Double>(11.5d).Test2",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.GenericFixture<Int32>(5).Test1",
                        Name = "NUnit.Tests.GenericFixture<Int32>(5).Test1",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.GenericFixture<Int32>(5).Test2",
                        Name = "NUnit.Tests.GenericFixture<Int32>(5).Test2",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.IgnoredFixture.Test1",
                        Name = "NUnit.Tests.IgnoredFixture.Test1",
                        Result = Result.Ignored,
                        Duration =-1,
                        ErrorMsg = ""
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.IgnoredFixture.Test2",
                        Name = "NUnit.Tests.IgnoredFixture.Test2",
                        Result = Result.Ignored,
                        Duration = -1,
                        ErrorMsg = ""
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.IgnoredFixture.Test3",
                        Name = "NUnit.Tests.IgnoredFixture.Test3",
                        Result = Result.Ignored,
                        Duration = -1,
                        ErrorMsg = ""
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.ParameterizedFixture(42).Test1",
                        Name = "NUnit.Tests.ParameterizedFixture(42).Test1",
                        Result = Result.Pass,
                        Duration = 0.0,
                        ErrorMsg = ""
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.ParameterizedFixture(42).Test2",
                        Name = "NUnit.Tests.ParameterizedFixture(42).Test2",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.ParameterizedFixture(5).Test1",
                        Name = "NUnit.Tests.ParameterizedFixture(5).Test1",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.ParameterizedFixture(5).Test2",
                        Name = "NUnit.Tests.ParameterizedFixture(5).Test2",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.Singletons.OneTestCase.TestCase",
                        Name = "NUnit.Tests.Singletons.OneTestCase.TestCase",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.TestAssembly.MockTestFixture.MyTest",
                        Name = "NUnit.Tests.TestAssembly.MockTestFixture.MyTest",
                        Result = Result.Pass,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
                new TestMethod
                    {
                        ClassName = "NUnit.Tests.TestAssembly.MockTestFixture.MyTest1",
                        Name = "NUnit.Tests.TestAssembly.MockTestFixture.MyTest1",
                        Result = Result.Inconclusive,
                        Duration = 0.000,
                        ErrorMsg = "",
                    },
            };
            return testRun;
        }

        #endregion

        public static TestRun NUnitBoolResultsDocument()
        {
            var testRun = new TestRun();
            testRun.Comment = "Nunit bool resulsts.";
            testRun.Name = @"E:\Program Files\NUnit V2.2\bin\mock-assembly.dll";
            testRun.TestsMethods = new List<TestMethod>()
            {
                new TestMethod
                {
                    ClassName ="NUnit.Tests.TestAssembly.MockTestFixture.MyTest",
                    Name ="NUnit.Tests.TestAssembly.MockTestFixture.MyTest",
                    Result = Result.Pass,
                    Duration = 0.01,
                    ErrorMsg = string.Empty
                },
                new TestMethod
                {
                    ClassName ="NUnit.Tests.Assemblies.MockTestFixture.MockTest1",
                    Name ="NUnit.Tests.Assemblies.MockTestFixture.MockTest1",
                    Result = Result.Pass,
                    Duration = 0.0,
                    ErrorMsg = string.Empty
                },
                new TestMethod
                {
                    ClassName ="NUnit.Tests.Assemblies.MockTestFixture.MockTest2",
                    Name ="NUnit.Tests.Assemblies.MockTestFixture.MockTest2",
                    Result = Result.Pass,
                    Duration = 0.0,
                    ErrorMsg = string.Empty
                },
                new TestMethod
                {
                    ClassName ="NUnit.Tests.Assemblies.MockTestFixture.MockTest3",
                    Name ="NUnit.Tests.Assemblies.MockTestFixture.MockTest3",
                    Result = Result.Pass,
                    Duration = 0.01,
                    ErrorMsg = string.Empty
                },
                new TestMethod
                {
                    ClassName ="NUnit.Tests.Assemblies.MockTestFixture.MockTest5",
                    Name ="NUnit.Tests.Assemblies.MockTestFixture.MockTest5",
                    Result = Result.NotRun,
                    Duration = -1,
                    ErrorMsg = "Method MockTest5's signature is not correct: it must be a public method."
                },
                new TestMethod
                {
                    ClassName ="NUnit.Tests.Assemblies.MockTestFixture.MockTest4",
                    Name ="NUnit.Tests.Assemblies.MockTestFixture.MockTest4",
                    Result = Result.NotRun,
                    Duration = -1,
                    ErrorMsg = "ignoring this test method for now"
                },
                new TestMethod
                {
                    ClassName ="NUnit.Tests.Assemblies.MockTestFixture.ExplicitlyRunTest",
                    Name ="NUnit.Tests.Assemblies.MockTestFixture.ExplicitlyRunTest",
                    Result = Result.NotRun,
                    Duration = -1,
                    ErrorMsg = "Explicit selection required"
                },
                new TestMethod
                {
                    ClassName ="NUnit.Tests.Singletons.OneTestCase.TestCase",
                    Name ="NUnit.Tests.Singletons.OneTestCase.TestCase",
                    Result = Result.Pass,
                    Duration = 0.0,
                    ErrorMsg = string.Empty
                },
                new TestMethod
                {
                    ClassName ="NUnit.Tests.Singletons.OneTestCase.TestCase1",
                    Name ="NUnit.Tests.Singletons.OneTestCase.TestCase1",
                    Result = Result.Inconclusive,
                    Duration = 0.0,
                    ErrorMsg = string.Empty
                },
                new TestMethod
                {
                    ClassName ="NUnit.Tests.Singletons.OneTestCase.TestCase3",
                    Name ="NUnit.Tests.Singletons.OneTestCase.TestCase3",
                    Result = Result.Inconclusive,
                    Duration = 0.0,
                    ErrorMsg = string.Empty
                },
            };
            return testRun;
        }
    }
}
