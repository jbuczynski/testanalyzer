﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests.Services
{
    using System.Diagnostics;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model.Enums;

    using Service;
    using Service.Parsers;

    [TestClass]
    public class ParserTest
    {
        #region Fields

        private IParserManagerService parserManagerService;

        private IXmlParserManager xmlParserManager;

        #endregion

        #region Test init

        [TestInitialize]
        public void SetUp()
        {
            xmlParserManager = new XmlParserManager();
            parserManagerService = new ParserManagerService(xmlParserManager);
        }

        #endregion

        #region JUnit tests

        [TestMethod]
        [TestCategory("JUnit")]
        public void TryParseJNUnitUnknowSuitsType_ShouldReturnNotNullTestRun()
        {
            var comment = "TestRun comment from integration test. JUnit.";

            var testRun = parserManagerService.TryParse(TestDataHelper.JUnitDocument, comment, TestSuitsType.Unknow);

            Assert.IsNotNull(testRun, "ParserManagerService can recognize type of xml.");
        }

        [TestMethod]
        [TestCategory("JUnit")]
        public void TryParseJUnitSuitsType_ShouldReturnNotNullTestRun()
        {
            var comment = "TestRun comment from integration test. JUnit.";

            var testRun = parserManagerService.TryParse(TestDataHelper.JUnitDocument, comment, TestSuitsType.JUnit);

            Assert.IsNotNull(testRun, "ParserManagerService has privided information about test suite type and can parse it.");
        }

        [TestMethod]
        [TestCategory("JUnit")]
        public void TryParseJUnitWithNUnitSuitsType_ShouldRecognizeTestSuiteType()
        {
            var comment = "TestRun comment from integration test. JUnit.";

            var testRun = parserManagerService.TryParse(TestDataHelper.JUnitDocument, comment, TestSuitsType.NUnit);

            Assert.IsNotNull(testRun, "ParserManagerService has wrong information about test suit type, but can recognize type of JUnit and can parse it.");
        }

        #endregion

        #region NUnit tests

        [TestMethod]
        [TestCategory("NUnit")]
        public void TryParseNUnitUnknowSuitsType_ShouldReturnNotNullTestRun()
        {
            var comment = "TestRun comment from integration test. NUnit.";

            var testRun = parserManagerService.TryParse(TestDataHelper.NUnitDocument, comment, TestSuitsType.Unknow);

            Assert.IsNotNull(testRun, "ParserManagerService can recognize type of xml.");
        }

        [TestMethod]
        [TestCategory("NUnit")]
        public void TryParseNUnitSuitsType_ShouldReturnNotNullTestRun()
        {
            var comment = "TestRun comment from integration test. NUnit.";

            var testRun = parserManagerService.TryParse(TestDataHelper.NUnitDocument, comment, TestSuitsType.NUnit);

            Assert.IsNotNull(testRun, "ParserManagerService has privided information about test suite type and can parse it.");
        }



        [TestMethod]
        [TestCategory("NUnit")]
        public void TryParseNUnitWithJUnitSuitsType_ShouldRecognizeTestSuiteType()
        {
            var comment = "TestRun comment from integration test. JUnit.";

            var testRun = parserManagerService.TryParse(TestDataHelper.NUnitDocument, comment, TestSuitsType.JUnit);

            Assert.IsNotNull(testRun, "ParserManagerService has wrong information about test suit type, but can recognize type of NUnit and can parse it.");
        }

        [TestMethod]
        [TestCategory("NUnit")]
        public void TryParseNUnitWithNUnitSuitsType_ShouldReturnTestRunWithValidContent()
        {
            var testData = TestDataHelper.GetTestRunBasedOnNUnit_testResult();

            var testRunResult = parserManagerService.TryParse(TestDataHelper.NUnitDocument, testData.Comment, TestSuitsType.NUnit);

            Assert.AreEqual(testData.Comment, testRunResult.Comment, "Comment are equal.");
            Assert.AreEqual(testData.Name, testRunResult.Name, "Name are equal.");
            Assert.IsNotNull(testRunResult.TestsMethods, "TestMethods in test run is not null");
            Assert.AreEqual(testRunResult.TestsMethods.Count, testData.TestsMethods.Count, "TestMethods count are equal.");
        }

        [TestMethod]
        [TestCategory("NUnit")]
        public void TryParseNUnitWithNUnitSuitsType_ShouldReturnTestRunWithTestMethotTheSameTestCasesAsTestSet()
        {
            var testData = TestDataHelper.GetTestRunBasedOnNUnit_testResult();

            var testRunResult = parserManagerService.TryParse(TestDataHelper.NUnitDocument, testData.Comment, TestSuitsType.NUnit);

            for (int i = 0; i < testRunResult.TestsMethods.Count; i++)
            {
                Assert.AreEqual(testData.TestsMethods[i].ClassName, testRunResult.TestsMethods[i].ClassName, "Test method ClassName are the same.");
                Assert.AreEqual(testData.TestsMethods[i].Duration, testRunResult.TestsMethods[i].Duration, "Test method Duration are the same.");
                Assert.AreEqual(testData.TestsMethods[i].ErrorMsg, testRunResult.TestsMethods[i].ErrorMsg, "Test method ErrorMsg are the same.");
                Assert.AreEqual(testData.TestsMethods[i].Name, testRunResult.TestsMethods[i].Name, "Test method Name are the same.");
                Assert.AreEqual(testData.TestsMethods[i].Result, testRunResult.TestsMethods[i].Result, "Test method Name are the same.");
            }
        }

        [TestMethod]
        [TestCategory("NUnit")]
        public void TryParseNUnitWithJUnitSuitsType_ShouldReturnTestRunWithValidContent()
        {
            var testData = TestDataHelper.GetTestRunBasedOnNUnit_testResult();

            var testRunResult = parserManagerService.TryParse(TestDataHelper.NUnitDocument, testData.Comment, TestSuitsType.JUnit);

            Assert.AreEqual(testData.Comment, testRunResult.Comment, "Comment are equal.");
            Assert.AreEqual(testData.Name, testRunResult.Name, "Name are equal.");
            Assert.IsNotNull(testRunResult.TestsMethods, "TestMethods in test run is not null");
            Assert.AreEqual(testRunResult.TestsMethods.Count, testData.TestsMethods.Count, "TestMethods count are equal.");
        }

        [TestMethod]
        [TestCategory("NUnit")]
        public void TryParseNUnitWithJUnitSuitsType_ShouldReturnTestRunWithTestMethotTheSameTestCasesAsTestSet()
        {
            var testData = TestDataHelper.GetTestRunBasedOnNUnit_testResult();

            var testRunResult = parserManagerService.TryParse(TestDataHelper.NUnitDocument, testData.Comment, TestSuitsType.JUnit);

            for (int i = 0; i < testRunResult.TestsMethods.Count; i++)
            {
                Assert.AreEqual(testData.TestsMethods[i].ClassName, testRunResult.TestsMethods[i].ClassName, "Test method ClassName are the same.");
                Assert.AreEqual(testData.TestsMethods[i].Duration, testRunResult.TestsMethods[i].Duration, "Test method Duration are the same.");
                Assert.AreEqual(testData.TestsMethods[i].ErrorMsg, testRunResult.TestsMethods[i].ErrorMsg, "Test method ErrorMsg are the same.");
                Assert.AreEqual(testData.TestsMethods[i].Name, testRunResult.TestsMethods[i].Name, "Test method Name are the same.");
                Assert.AreEqual(testData.TestsMethods[i].Result, testRunResult.TestsMethods[i].Result, "Test method Name are the same.");
            }
        }

        [TestMethod]
        [TestCategory("NUnit")]
        public void TryParseNUnitWithUnknowSuitsType_ShouldReturnTestRunWithValidContent()
        {
            var testData = TestDataHelper.GetTestRunBasedOnNUnit_testResult();

            var testRunResult = parserManagerService.TryParse(TestDataHelper.NUnitDocument, testData.Comment, TestSuitsType.Unknow);

            Assert.AreEqual(testData.Comment, testRunResult.Comment, "Comment are equal.");
            Assert.AreEqual(testData.Name, testRunResult.Name, "Name are equal.");
            Assert.IsNotNull(testRunResult.TestsMethods, "TestMethods in test run is not null");
            Assert.AreEqual(testRunResult.TestsMethods.Count, testData.TestsMethods.Count, "TestMethods count are equal.");
        }

        [TestMethod]
        [TestCategory("NUnit")]
        public void TryParseNUnitWithUnknowSuitsType_ShouldReturnTestRunWithTestMethotTheSameTestCasesAsTestSet()
        {
            var testData = TestDataHelper.GetTestRunBasedOnNUnit_testResult();

            var testRunResult = parserManagerService.TryParse(TestDataHelper.NUnitDocument, testData.Comment, TestSuitsType.Unknow);

            for (int i = 0; i < testRunResult.TestsMethods.Count; i++)
            {
                Assert.AreEqual(testData.TestsMethods[i].ClassName, testRunResult.TestsMethods[i].ClassName, "Test method ClassName are the same.");
                Assert.AreEqual(testData.TestsMethods[i].Duration, testRunResult.TestsMethods[i].Duration, "Test method Duration are the same.");
                Assert.AreEqual(testData.TestsMethods[i].ErrorMsg, testRunResult.TestsMethods[i].ErrorMsg, "Test method ErrorMsg are the same.");
                Assert.AreEqual(testData.TestsMethods[i].Name, testRunResult.TestsMethods[i].Name, "Test method Name are the same.");
                Assert.AreEqual(testData.TestsMethods[i].Result, testRunResult.TestsMethods[i].Result, "Test method Name are the same.");
            }
        }

        #endregion

        #region Unknow xml tests

        [TestMethod]
        [TestCategory("Unknow")]
        public void TryParseInvalidXmlAndUnknowSuitsType_ShouldReturnNullTestRun()
        {
            var comment = "TestRun comment from integration test. Invalid xml.";

            var testRun = parserManagerService.TryParse(TestDataHelper.InvalidXmlDocument, comment, TestSuitsType.Unknow);

            Assert.IsNull(testRun, "ParserManagerService cannot recognize type of xml.");
        }

        #endregion
    }
}
