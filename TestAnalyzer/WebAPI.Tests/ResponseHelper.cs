﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests
{
    using System.Net.Http;

    public class ResponseHelper
    {
        public static T GetContentFromResponseMessage<T>(HttpResponseMessage responseMessage)
        {
            var readTask = responseMessage.Content.ReadAsAsync<T>();
            readTask.Wait();
            T testRunResponse = readTask.Result;
            return testRunResponse;
        }

        public static T GetHttpResponseMessageFromTask<T>(Task<T> task)
        {
            task.Wait();
            T result = task.Result;
            return result;
        }
    }
}
