﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests.Services
{
    using System.Runtime.Remoting.Metadata.W3cXsd2001;
    using System.Xml;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model;
    using Model.Enums;

    using Moq;

    using Service;
    using Service.Parsers;

    [TestClass]
    public class ParserManagerServiceTest
    {
        #region Tests

        [TestMethod]
        public void TryParseNUnitXmlWithUnknowType_ShouldReturnNotNullTestRun()
        {
            var xmlParserManagerMock = new Mock<IXmlParserManager>();
            var nUnitParserMock = new Mock<IXmlParser>();
            var jUnitParserMock = new Mock<IXmlParser>();

            nUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(new TestRun());
            jUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(default(TestRun));

            xmlParserManagerMock.Setup(c => c.NUnitParser).Returns(nUnitParserMock.Object);
            xmlParserManagerMock.Setup(c => c.JUnitParser).Returns(jUnitParserMock.Object);

            var parserManagerService = new ParserManagerService(xmlParserManagerMock.Object);

            var testRun = parserManagerService.TryParse(TestDataHelper.NUnitDocument, string.Empty);

            Assert.IsNotNull(testRun, "Method TryParse should return not null TestRun.");
        }

        [TestMethod]
        public void TryParseJUnitXmlWithUnknowType_ShouldReturnNotNullTestRun()
        {
            var xmlParserManagerMock = new Mock<IXmlParserManager>();
            var nUnitParserMock = new Mock<IXmlParser>();
            var jUnitParserMock = new Mock<IXmlParser>();

            nUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(default(TestRun));
            jUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(new TestRun());

            xmlParserManagerMock.Setup(c => c.NUnitParser).Returns(nUnitParserMock.Object);
            xmlParserManagerMock.Setup(c => c.JUnitParser).Returns(jUnitParserMock.Object);

            var parserManagerService = new ParserManagerService(xmlParserManagerMock.Object);

            var testRun = parserManagerService.TryParse(TestDataHelper.JUnitDocument, string.Empty);

            Assert.IsNotNull(testRun, "Method TryParse should return not null TestRun.");
        }

        [TestMethod]
        public void TryParseUnknowXmlWithUnknowType_ShouldNull()
        {
            var xmlParserManagerMock = new Mock<IXmlParserManager>();
            var nUnitParserMock = new Mock<IXmlParser>();
            var jUnitParserMock = new Mock<IXmlParser>();

            nUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(default(TestRun));
            jUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(default(TestRun));

            xmlParserManagerMock.Setup(c => c.NUnitParser).Returns(nUnitParserMock.Object);
            xmlParserManagerMock.Setup(c => c.JUnitParser).Returns(jUnitParserMock.Object);


            var parserManagerService = new ParserManagerService(xmlParserManagerMock.Object);

            var testRun = parserManagerService.TryParse(TestDataHelper.InvalidXmlDocument, string.Empty);

            Assert.IsNull(testRun, "Method TryParse should return null TestRun, because we xml cannot be parsed to any type of test run.");
        }

        [TestMethod]
        public void TryParseNUnitXmlWithNUnitType_ShouldReturnNotNullTestRun()
        {
            var xmlParserManagerMock = new Mock<IXmlParserManager>();
            var nUnitParserMock = new Mock<IXmlParser>();

            nUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(default(TestRun));
            xmlParserManagerMock.Setup(c => c.NUnitParser).Returns(nUnitParserMock.Object);

            var parserManagerService = new ParserManagerService(xmlParserManagerMock.Object);

            var testRun = parserManagerService.TryParse(TestDataHelper.NUnitDocument, string.Empty, TestSuitsType.NUnit);

            Assert.IsNull(testRun, "Method TryParse with TestSuitsType == NUnit should return not null TestRun.");
        }

        [TestMethod]
        public void TryParseInvalidNUnitXmlWithNUnitType_ShouldReturnNullTestRun()
        {
            var xmlParserManagerMock = new Mock<IXmlParserManager>();
            var nUnitParserMock = new Mock<IXmlParser>();

            nUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(default(TestRun));
            xmlParserManagerMock.Setup(c => c.NUnitParser).Returns(nUnitParserMock.Object);

            var parserManagerService = new ParserManagerService(xmlParserManagerMock.Object);

            var testRun = parserManagerService.TryParse(TestDataHelper.InvalidXmlDocument, string.Empty, TestSuitsType.NUnit);

            Assert.IsNull(testRun, "Method TryParse invalid xml TestSuitsType == NUnit should return null.");
        }

        [TestMethod]
        public void TryParseJUnitAsNUnitXmlWithNUnitType_ShouldReturnNullTestRun()
        {
            var xmlParserManagerMock = new Mock<IXmlParserManager>();
            var nUnitParserMock = new Mock<IXmlParser>();

            nUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(default(TestRun));
            xmlParserManagerMock.Setup(c => c.NUnitParser).Returns(nUnitParserMock.Object);

            var parserManagerService = new ParserManagerService(xmlParserManagerMock.Object);

            var testRun = parserManagerService.TryParse(TestDataHelper.JUnitDocument, string.Empty, TestSuitsType.NUnit);

            Assert.IsNull(testRun, "Method TryParse JUnit xml as NUnit xml TestSuitsType == NUnit should return not TestRun.");
        }

        [TestMethod]
        public void TryParseJUnitXmlWithJUnitType_ShouldReturnNotNullTestRun()
        {
            var xmlParserManagerMock = new Mock<IXmlParserManager>();
            var jUnitParserMock = new Mock<IXmlParser>();

            jUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(new TestRun());
            xmlParserManagerMock.Setup(c => c.JUnitParser).Returns(jUnitParserMock.Object);

            var parserManagerService = new ParserManagerService(xmlParserManagerMock.Object);

            var testRun = parserManagerService.TryParse(TestDataHelper.JUnitDocument, string.Empty, TestSuitsType.JUnit);

            Assert.IsNotNull(testRun, "Method TryParse with TestSuitsType == JUnit should return not null TestRun.");
        }

        [TestMethod]
        public void TryParseInvalidJUnitXmlWithJUnitType_ShouldReturnNull()
        {
            var xmlParserManagerMock = new Mock<IXmlParserManager>();
            var jUnitParserMock = new Mock<IXmlParser>();

            jUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(default(TestRun));
            xmlParserManagerMock.Setup(c => c.JUnitParser).Returns(jUnitParserMock.Object);

            var parserManagerService = new ParserManagerService(xmlParserManagerMock.Object);

            var testRun = parserManagerService.TryParse(TestDataHelper.InvalidXmlDocument, string.Empty, TestSuitsType.JUnit);

            Assert.IsNull(testRun, "Method TryParse invalid xml with TestSuitsType == JUnit should return null TestRun.");
        }

        [TestMethod]
        public void TryParseNUnitAsJUnitXmlWithNUnitType_ShouldReturnNull()
        {
            var xmlParserManagerMock = new Mock<IXmlParserManager>();
            var jUnitParserMock = new Mock<IXmlParser>();

            jUnitParserMock.Setup(c => c.Parse(It.IsAny<XmlDocument>(), It.IsAny<string>())).Returns(default(TestRun));
            xmlParserManagerMock.Setup(c => c.JUnitParser).Returns(jUnitParserMock.Object);

            var parserManagerService = new ParserManagerService(xmlParserManagerMock.Object);

            var testRun = parserManagerService.TryParse(TestDataHelper.NUnitDocument, string.Empty, TestSuitsType.JUnit);

            Assert.IsNull(testRun, "Method TryParse JUnit xml as NUnit xml should return not TestRun.");
        }

        #endregion
    }
}
