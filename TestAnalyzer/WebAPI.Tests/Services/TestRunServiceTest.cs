﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests.Services
{
    using System.Linq.Expressions;

    using DAL.Common;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model;
    using Model.Enums;

    using Moq;

    using Service;

    [TestClass]
    public class TestRunServiceTest
    {
        #region Fields - mock

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Mock<IUserService> userServiceMock;

        #endregion

        #region Set up

        [TestInitialize]
        public void SetUp()
        {
            this.unitOfWorkMock = new Mock<IUnitOfWork>();
            this.userServiceMock = new Mock<IUserService>();
        }

        #endregion

        #region Service method tests

        [TestMethod]
        public void TestRunServiceInitCorrectly()
        {
            try
            {
                var testRunService = new TestRunService(this.unitOfWorkMock.Object, this.userServiceMock.Object);

                Assert.IsNotNull(testRunService);
                Assert.IsInstanceOfType(testRunService, typeof(ITestRunService), $"{nameof(TestRunService)} implements {nameof(ITestRunService)} interface");
            }
            catch (Exception ex)
            {
                Assert.Fail($"Cannot create instance of {nameof(TestRunService)}. Message: {ex.Message}");
            }
        }

        [TestMethod]
        public void GetByIdWithUserReturnNotNull()
        {
            var testRuns = this.GetTestRuns().AsQueryable();

            this.userServiceMock.Setup(c => c.GetUserId()).Returns("123-123-132");

            this.unitOfWorkMock.Setup(
                c => c.TestRunRepository.Get(It.IsAny<Expression<Func<TestRun, bool>>>(),
                It.IsAny<Func<IQueryable<TestRun>, IOrderedQueryable<TestRun>>>(),
                It.IsAny<string>()))
                .Returns((Expression<Func<TestRun, bool>> predicate, Func<IQueryable<TestRun>, IOrderedQueryable<TestRun>> func, string value) => testRuns.Where(predicate));

            foreach (var id in testRuns.Select(c => c.Id))
            {
                var result = this.InstanceOfTestRunService.GetById(id);
                var testRunMock = testRuns.FirstOrDefault(c => c.Id == id);

                Assert.IsNotNull(result, "Result from service is not null");
                Assert.IsNotNull(testRunMock, "Mock object to compare is not null");
                Assert.AreEqual(testRunMock.Comment, result.Comment);
                Assert.AreEqual(testRunMock.UploadType, result.UploadType);
                Assert.AreEqual(testRunMock.Name, result.Name);
                Assert.AreEqual(testRunMock.Id, result.Id);
                Assert.AreEqual(testRunMock.Added, result.Added);
                Assert.AreEqual(testRunMock.Modified, result.Modified);
            }
        }

        [TestMethod]
        public void GetTestRunsReturnCollection()
        {
            var testRunsMock = this.GetTestRuns();

            this.userServiceMock.Setup(c => c.GetUserId()).Returns("123-123-132");

            this.unitOfWorkMock.Setup(c => c.TestRunRepository
                                .Get(It.IsAny<Expression<Func<TestRun, bool>>>(),
                                        It.IsAny<Func<IQueryable<TestRun>, IOrderedQueryable<TestRun>>>(),
                                        It.IsAny<string>()))
                                .Returns(testRunsMock);

            var result = this.InstanceOfTestRunService.GetTestRuns();

            Assert.IsNotNull(result, "Result from service is not null");
            Assert.AreEqual(testRunsMock.Count, result.Count());
        }

        [TestMethod]
        public void CreateAndSaveCorrectlySaveDataAndReturnTheSameObject()
        {
            var testRun = this.GetTestRun();
            this.userServiceMock.Setup(c => c.GetUserId()).Returns("123-123-132");
            this.unitOfWorkMock.Setup(c => c.TestRunRepository.Insert(It.IsAny<TestRun>()));
            this.unitOfWorkMock.Setup(c => c.TestRunRepository.Save());

            var result = this.InstanceOfTestRunService.CreateAndSave(testRun);

            Assert.IsNotNull(result);
            Assert.AreSame(testRun, result);
        }

        [TestMethod]
        public void UpdateCorrectlyReturnTheSameObject()
        {
            var oldTestRun = this.GetTestRun();
            var updateTestRun = this.GetTestRun();
            updateTestRun.Comment = "Comment update";

            this.unitOfWorkMock.Setup(c => c.TestRunRepository.Update(It.IsAny<TestRun>()));
            this.unitOfWorkMock.Setup(c => c.TestRunRepository.Save());

            var result = this.InstanceOfTestRunService.Update(oldTestRun, updateTestRun);

            Assert.IsNotNull(result);
            Assert.AreSame(oldTestRun, result);
            Assert.AreEqual(updateTestRun.UploadType, result.UploadType);
            Assert.AreEqual(updateTestRun.Comment, result.Comment);
            Assert.AreEqual(updateTestRun.Name, result.Name);
        }

        [TestMethod]
        public void DeleteSuccess()
        {
            var oldTestRun = this.GetTestRun();

            this.unitOfWorkMock.Setup(c => c.TestRunRepository.Delete(It.IsAny<TestRun>()));
            this.unitOfWorkMock.Setup(c => c.TestRunRepository.Save());

            try
            {
                this.InstanceOfTestRunService.Delete(oldTestRun);
            }
            catch (Exception ex)
            {
                Assert.Fail($"Cannot delete Test Run. Message: {ex}");
            }
        }

        #endregion

        #region Helpers

        private TestRunService InstanceOfTestRunService
        {
            get
            {
                var runService = new TestRunService(this.unitOfWorkMock.Object, this.userServiceMock.Object);
                return runService;
            }
        }

        private TestRun GetTestRun()
        {
            ApplicationUser user = new ApplicationUser() { Id = "123-123-132" };
            TestRun testRun = new TestRun
            {
                Comment = "Test run",
                Added = DateTime.Now,
                Modified = DateTime.Now,
                Name = "Test run name 1",
                Id = 1,
                UploadType = UploadType.Ci,
                User = user
            };


            return testRun;
        }

        private List<TestRun> GetTestRuns()
        {
            ApplicationUser user = new ApplicationUser() { Id = "123-123-132" };

            TestRun testRun = new TestRun
            {
                Comment = "Test run",
                Added = DateTime.Now,
                Modified = DateTime.Now,
                Name = "Test run name 1",
                UploadType = UploadType.Ci,
                Id = 1,
                User = user
            };
            TestRun testRun1 = new TestRun
            {
                Comment = "Test run",
                Added = DateTime.Now,
                Name = "Test run name 2",
                UploadType = UploadType.Ci,
                Id = 2,
                User = user
            };
            TestRun testRun2 = new TestRun
            {
                Comment = "Test run",
                Added = DateTime.Now,
                Name = "Test run name 3",
                UploadType = UploadType.User,
                Id = 3,
                User = user
            };
            return new List<TestRun> { testRun, testRun1, testRun2 };
        }

        #endregion
    }
}
