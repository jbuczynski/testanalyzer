﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests.Services
{
    using System.Xml;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model;
    using Model.Enums;

    using Service;
    using Service.Helpers;
    using Service.Parsers;
    using Service.TestSuits.NUnit;

    [TestClass]
    public class NUnitXmlParserServiceTest
    {
        private const string InvalidXml = @"<?xml version=""1.0"" encoding=""utf - 8"" standalone=""no""?><test-resulted></test-resulted>";

        private const string TestXmlPath = "Data/NUnit_testResult.xml";

        private XmlDocument xmlDocument;

        private IXmlParser nUnitXmlParser;

        [TestInitialize]
        public void SetUp()
        {
            this.xmlDocument = new XmlDocument();
            this.xmlDocument.Load(TestXmlPath);
            this.nUnitXmlParser = new NUnitXmlParserService();
        }

        [TestMethod]
        public void PassNullXmlDocument_ShouldReturnNull()
        {
            var result = this.nUnitXmlParser.Parse(null, string.Empty);

            Assert.IsNull(result, "Returned test run is null as expected");
        }

        [TestMethod]
        public void PassInvalidNotNullXml_ShouldReturNullTestRun()
        {
            var xml = new XmlDocument();
            xml.LoadXml(InvalidXml);
            var result = this.nUnitXmlParser.Parse(xml, "Comment");

            Assert.IsNull(result, "Parse xml to resultType object should throw exception and method Parse should return null TestRun.");
        }

        [TestMethod]
        public void PassValidXml_ShouldReturNotNullTestRun()
        {
            var result = this.nUnitXmlParser.Parse(this.xmlDocument, "Comment");

            Assert.IsNotNull(result, "Returned test run is not null.");
        }

        [TestMethod]
        public void PassValidXml_ShouldRetrunTestRunWithCollectionOfTestMethod()
        {
            var testRun = TestDataHelper.GetTestRunBasedOnNUnit_testResult();

            var result = this.nUnitXmlParser.Parse(this.xmlDocument, testRun.Comment);

            Assert.IsNotNull(result, "Returned test run is not null.");
            Assert.AreEqual(testRun.Comment, result.Comment, "Comment are equal.");
            for (int i = 0; i < result.TestsMethods.Count; i++)
            {
                var retrieved = result.TestsMethods[i];
                var expected = testRun.TestsMethods[i];

                Assert.AreEqual(expected.ErrorMsg, retrieved.ErrorMsg, "Error messages are equal.");
                Assert.AreEqual(expected.ClassName, retrieved.ClassName, "Class name of test method are equal.");
                Assert.AreEqual(expected.Duration, retrieved.Duration, "Duration time are the same");
                Assert.AreEqual(expected.Name, retrieved.Name, "Test method name are the same");
                Assert.AreEqual(expected.Result, retrieved.Result, "Result of test are the same.");
                Assert.IsNull(retrieved.MethodParameters, "No Method parameters for NUnit parser.");
                Assert.IsNotNull(retrieved.TestRun, "TestRun is assign to created TestMethod object. Also TestRun object contains list of TestMethods.");
                Assert.IsTrue(retrieved.TestRunId == 0, "TestRunIs set default by EF while saving data in db. In this stage should be 0.");
            }
        }

        [TestMethod]
        public void PassValidXml_ShouldRetrunNotNullListOfTestMethods()
        {
            var result = this.nUnitXmlParser.Parse(this.xmlDocument, string.Empty);

            Assert.IsNotNull(result.TestsMethods, "TestMethods in not null");
            Assert.IsTrue(result.TestsMethods.Count > 0, "TestMethods list contains elements.");
        }

        [TestMethod]
        public void PassValidXmlWithBoolTestResult_shouldReturnProperTestResults()
        {
            var testRun = TestDataHelper.NUnitBoolResultsDocument();
            this.xmlDocument = XmlHelper.LoadXmlDocument("Data/NUnit_boolResults.xml");
            var result = this.nUnitXmlParser.Parse(this.xmlDocument, testRun.Comment);

            Assert.IsNotNull(result, "Returned test run is not null.");
            Assert.AreEqual(testRun.Comment, result.Comment, "Comment are equal.");
            for (int i = 0; i < result.TestsMethods.Count; i++)
            {
                var retrieved = result.TestsMethods[i];
                var expected = testRun.TestsMethods[i];

                Assert.AreEqual(expected.ErrorMsg, retrieved.ErrorMsg, "Error messages are equal.");
                Assert.AreEqual(expected.ClassName, retrieved.ClassName, "Class name of test method are equal.");
                Assert.AreEqual(expected.Duration, retrieved.Duration, "Duration time are the same");
                Assert.AreEqual(expected.Name, retrieved.Name, "Test method name are the same");
                Assert.AreEqual(expected.Result, retrieved.Result, "Result of test are the same.");
                Assert.IsNull(retrieved.MethodParameters, "No Method parameters for NUnit parser.");
                Assert.IsNotNull(retrieved.TestRun, "TestRun is assign to created TestMethod object. Also TestRun object contains list of TestMethods.");
                Assert.IsTrue(retrieved.TestRunId == 0, "TestRunIs set default by EF while saving data in db. In this stage should be 0.");
            }
        }
    }
}
