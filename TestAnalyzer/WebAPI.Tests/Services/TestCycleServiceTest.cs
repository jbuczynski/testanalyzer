﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests.Services
{
    using System.Linq.Expressions;

    using DAL.Common;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model;

    using Moq;

    using Service;

    [TestClass]
    public class TestCycleServiceTest
    {
        #region Fields - mock

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Mock<IUserService> userServiceMock;

        #endregion

        #region Set up

        [TestInitialize]
        public void SetUp()
        {
            this.unitOfWorkMock = new Mock<IUnitOfWork>();
            this.userServiceMock = new Mock<IUserService>();
        }

        #endregion

        #region Service method tests

        [TestMethod]
        public void TestCycleServiceInitCorrectly()
        {
            try
            {
                var testCycleService = new TestCycleService(this.unitOfWorkMock.Object, this.userServiceMock.Object);

                Assert.IsNotNull(testCycleService);
                Assert.IsInstanceOfType(testCycleService, typeof(ITestCycleService), $"{nameof(TestCycleService)} implements {nameof(ITestCycleService)} interface");
            }
            catch (Exception ex)
            {
                Assert.Fail($"Cannot create instance of {nameof(TestCycleService)}. Message: {ex.Message}");
            }
        }

        [TestMethod]
        public void GetByIdWithUserReturnNotNull()
        {
            var testCyclesMock = this.GetTestCycles();

            this.userServiceMock.Setup(c => c.GetUserId()).Returns("123-123-132");

            this.unitOfWorkMock.Setup(c => c.TestCycleRepository
                                .Get(It.IsAny<Expression<Func<TestCycle, bool>>>(),
                                        It.IsAny<Func<IQueryable<TestCycle>, IOrderedQueryable<TestCycle>>>(),
                                        It.IsAny<string>()))
                                .Returns(testCyclesMock);

            int id = 1;
            TestCycle result = this.InstanceOfTestCycleService.GetById(id);
            var testCycleMock = testCyclesMock.FirstOrDefault(c => c.Id == id);

            Assert.IsNotNull(result, "Result from service is not null");
            Assert.IsNotNull(testCycleMock, "Mock object to compare is not null");
            Assert.AreEqual(testCycleMock.Comment, result.Comment);
            Assert.AreEqual(testCycleMock.IsApproved, result.IsApproved);
            Assert.AreEqual(testCycleMock.Name, result.Name);
            Assert.AreEqual(testCycleMock.Id, result.Id);
            Assert.AreEqual(testCycleMock.Added, result.Added);
            Assert.AreEqual(testCycleMock.Modified, result.Modified);
        }

        [TestMethod]
        public void GetTestCyclesReturnCollection()
        {
            var testCyclesMock = this.GetTestCycles();

            this.userServiceMock.Setup(c => c.GetUserId()).Returns("123-123-132");

            this.unitOfWorkMock.Setup(c => c.TestCycleRepository
                                .Get(It.IsAny<Expression<Func<TestCycle, bool>>>(),
                                        It.IsAny<Func<IQueryable<TestCycle>, IOrderedQueryable<TestCycle>>>(),
                                        It.IsAny<string>()))
                                .Returns(testCyclesMock);

            var result = this.InstanceOfTestCycleService.GetTestCycles();

            Assert.IsNotNull(result, "Result from service is not null");
            Assert.AreEqual(testCyclesMock.Count, result.Count());
        }

        [TestMethod]
        public void CreateAndSaveCorrectlySaveDataAndReturnTheSameObject()
        {
            var testCycle = this.GetTestCycleMock();
            this.userServiceMock.Setup(c => c.GetUserId()).Returns("123-123-132");
            this.unitOfWorkMock.Setup(c => c.TestCycleRepository.Insert(It.IsAny<TestCycle>()));
            this.unitOfWorkMock.Setup(c => c.TestCycleRepository.Save());

            var result = this.InstanceOfTestCycleService.CreateAndSave(testCycle);

            Assert.IsNotNull(result);
            Assert.AreSame(testCycle, result);
        }

        [TestMethod]
        public void UpdateCorrectlyReturnTheSameObject()
        {
            var oldTestCycle = this.GetTestCycleMock();
            var updatedTestCycle = this.GetTestCycleMock();
            updatedTestCycle.Comment = "Comment update";

            this.unitOfWorkMock.Setup(c => c.TestCycleRepository.Update(It.IsAny<TestCycle>()));
            this.unitOfWorkMock.Setup(c => c.TestCycleRepository.Save());

            var result = this.InstanceOfTestCycleService.Update(oldTestCycle, updatedTestCycle);

            Assert.IsNotNull(result);
            Assert.AreSame(oldTestCycle, result);
            Assert.AreEqual(updatedTestCycle.Comment, result.Comment);
            Assert.AreEqual(updatedTestCycle.IsApproved, result.IsApproved);
            Assert.AreEqual(updatedTestCycle.Name, result.Name);
        }

        [TestMethod]
        public void DeleteSuccess()
        {
            var oldTestCycle = this.GetTestCycleMock();

            this.unitOfWorkMock.Setup(c => c.TestCycleRepository.Delete(It.IsAny<TestCycle>()));
            this.unitOfWorkMock.Setup(c => c.TestCycleRepository.Save());

            try
            {
                this.InstanceOfTestCycleService.Delete(oldTestCycle);
            }
            catch (Exception ex)
            {
                Assert.Fail($"Cannot delete Test Cycle. Message: {ex}");
            }
        }

        #endregion

        #region Helpers

        private TestCycleService InstanceOfTestCycleService
        {
            get
            {
                var testCycleService = new TestCycleService(this.unitOfWorkMock.Object, this.userServiceMock.Object);
                return testCycleService;
            }
        }

        private TestCycle GetTestCycleMock()
        {
            ApplicationUser user = new ApplicationUser() { Id = "123-123-123" };
            TestCycle testCycle = new TestCycle
            {
                Comment = "Test cycle",
                Added = DateTime.Now,
                Modified = DateTime.Now,
                Name = "Test cycle name 1",
                Id = 1,
                IsApproved = true,
                User = user
            };


            return testCycle;
        }

        private List<TestCycle> GetTestCycles()
        {
            ApplicationUser user = new ApplicationUser() { Id = "123-123-123" };

            TestCycle testCycle = new TestCycle
            {
                Comment = "Test cycle",
                Added = DateTime.Now,
                Modified = DateTime.Now,
                Name = "Test cycle name 1",
                Id = 1,
                IsApproved = true,
                User = user
            };
            TestCycle testCycle1 = new TestCycle
            {
                Comment = "Test cycle",
                Added = DateTime.Now,
                Name = "Test cycle name 2",
                Id = 2,
                IsApproved = false,
                User = user
            };
            TestCycle testCycle2 = new TestCycle
            {
                Comment = "Test cycle",
                Added = DateTime.Now,
                Name = "Test cycle name 3",
                Id = 3,
                IsApproved = true,
                User = user
            };
            return new List<TestCycle> { testCycle, testCycle1, testCycle2 };
        }

        #endregion
    }
}
