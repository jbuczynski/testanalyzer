﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests.Services
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Service.Parsers;

    [TestClass]
    public class XmlParserManagerTest
    {
        #region Fiels

        private IXmlParserManager _xmlParserManager;

        #endregion

        #region TestInit

        [TestInitialize]
        public void SetUp()
        {
            this._xmlParserManager = new XmlParserManager();
        }

        #endregion

        #region Tests

        [TestMethod]
        public void GetJUnitParser_shouldReturnInstanceOfJUnitXmlParserService()
        {
            IXmlParser jUnitParser = this._xmlParserManager.JUnitParser;

            Assert.IsNotNull(jUnitParser, "Instance of JUnitXmlParserService is not null");
            Assert.IsInstanceOfType(jUnitParser, typeof(JUnitXmlParserService), "jUnitParser is typeof JUnitXmlParserService");
        }

        [TestMethod]
        public void GetNUnitParser_shouldReturnInstanceOfNUnitXmlParserService()
        {
            IXmlParser unitParser = this._xmlParserManager.NUnitParser;

            Assert.IsNotNull(unitParser, "Instance of NUnitXmlParserService is not null");
            Assert.IsInstanceOfType(unitParser, typeof(NUnitXmlParserService), "jUnitParser is typeof NUnitXmlParserService");
        }

        [TestMethod]
        public void GetNUnitParser_AlwaysShouldReturnTheSameInstance()
        {
            IXmlParser firstParser = this._xmlParserManager.NUnitParser;
            IXmlParser secondParser = this._xmlParserManager.NUnitParser;

            Assert.AreSame(firstParser, secondParser, "Always has the same instance of parser.");
        }

        [TestMethod]
        public void GetJUnitParser_AlwaysShouldReturnTheSameInstance()
        {
            IXmlParser firstParser = this._xmlParserManager.JUnitParser;
            IXmlParser secondParser = this._xmlParserManager.JUnitParser;

            Assert.AreSame(firstParser, secondParser, "Always has the same instance of parser.");
        } 
        #endregion
    }
}
