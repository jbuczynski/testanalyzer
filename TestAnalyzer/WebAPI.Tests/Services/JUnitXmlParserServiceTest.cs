﻿namespace WebAPI.Tests.Services
{
    using System.Xml;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model;

    using Service;
    using Service.Parsers;

    [TestClass]
    public class JUnitXmlParserServiceTest
    {
        [TestMethod]
        [DeploymentItem("Data", "Data")]
        public void Parse()
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.Load("Data/nose2.xml");
            IXmlParserManager manager = new XmlParserManager();
            ParserManagerService ExP1 = new ParserManagerService(manager);
            TestRun wynik = ExP1.TryParse(xmlDocument, "Comment");
            Assert.IsNotNull(wynik);
        }
    }
}
