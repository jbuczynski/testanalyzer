﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//namespace WebAPI.Tests.Services
//{
//    using System.Linq.Expressions;
//    using System.Security.Claims;
//    using System.Security.Principal;
//    using System.Web;
//    using System.Web.Security;
//    using DAL.Common;
//    using Microsoft.AspNet.Identity;
//    using Microsoft.VisualStudio.TestTools.UnitTesting;
//    using Model;
//    using Moq;
//    using Service;
//    [TestClass]
//    public class UserServiceTest
//    {
//        [TestMethod]
//        public void PropetyInitUserService()
//        {
//            try
//            {
//                var unitOfWorkMock = new Mock<IUnitOfWork>();
//                var userService = new UserService(unitOfWorkMock.Object);
//                Assert.IsNotNull(userService);
//                Assert.IsInstanceOfType(userService, typeof(IUserService));
//            }
//            catch (Exception ex)
//            {
//                Assert.Fail($"Cannot create instance of {nameof(UserService)}. Message: {ex.Message}");
//            }
//        }
//        [TestMethod]
//        public void GetUserIdReturnUserId()
//        {
//            var unitOfWorkMock = new Mock<IUnitOfWork>();
//            var context = new Mock<HttpContextBase>();
//            var mockIdentity = new Mock<IIdentity>();
//            context.SetupGet(x => x.User.Identity).Returns(mockIdentity.Object);
//            mockIdentity.Setup(x => x.GetUserName()).Returns("User Id");
//            var userService = new UserService(unitOfWorkMock.Object);

//            var resultId = userService.GetUserId();

//            Assert.IsNotNull(resultId);
//        }


//        [TestMethod]
//        public void GetUserReturnUser()
//        {
//            var user = new ApplicationUser { Id = "1", UserName = "UserName", Email = "mail@email.com", };
//            var unitOfWorkMock = new Mock<IUnitOfWork>();
//            unitOfWorkMock.Setup(c => c.ApplicationUserRepository.GetById(It.IsAny<object>())).Returns(user);
//            //var context = new Mock<HttpContextBase>();
//            //var mockIdentity = new Mock<IIdentity>();
//            //context.SetupGet(x => x.User.Identity).Returns(mockIdentity.Object);
//            //mockIdentity.Setup(x => x.Name).Returns("UserName");
//            var userService = new UserService(unitOfWorkMock.Object);
//            var claim = new Claim("test", "IdOfYourChoosing");
//            var mockIdentity =
//                Mock.Of<ClaimsIdentity>(ci => ci.FindFirst(It.IsAny<string>()) == claim);

//            var applicationUser = userService.GetUser();

//            Assert.IsNotNull(applicationUser);
//            Assert.AreEqual(user.Id, applicationUser.Id);
//            Assert.AreEqual(user.UserName, applicationUser.UserName);
//            Assert.AreEqual(user.Email, applicationUser.Email);
//        }
//    }

//    public class CurrentUser
//    {
//        public CurrentUser(IIdentity identity)
//        {
//            IsAuthenticated = identity.IsAuthenticated;
//            DisplayName = identity.Name;

//            var formsIdentity = identity as FormsIdentity;

//            if (formsIdentity != null)
//            {
//                UserID = int.Parse(formsIdentity.Ticket.UserData);
//            }
//        }

//        public string DisplayName { get; private set; }
//        public bool IsAuthenticated { get; private set; }
//        public int UserID { get; private set; }
//    }
//}
