﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests.Controllers
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model;
    using Model.Enums;

    using Moq;

    using Service;

    using WebAPI.Controllers;

    [TestClass]
    public class TestCycleControllerTest
    {
        #region Fields - Mocks

        private Mock<ITestCycleService> testCycleServiceMock;

        #endregion

        #region Set up

        /// <summary>
        /// The set up.
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            this.testCycleServiceMock = new Mock<ITestCycleService>();
        }

        #endregion

        #region Controller constructor - test

        [TestMethod]
        public void PropertyInitializeController()
        {
            try
            {
                var controller = PrepareController(this.testCycleServiceMock);

                Assert.IsNotNull(controller);
                Assert.IsInstanceOfType(controller, typeof(TestCycleController));
            }
            catch (Exception ex)
            {
                Assert.Fail($"Cannot create instance of {nameof(TestCycleController)}. Message: ", ex);
            }
        }

        #endregion

        #region Get all test runs - tests

        [TestMethod]
        public void GetAllTestCyclesReturnNotNull()
        {
            this.testCycleServiceMock.Setup(c => c.GetTestCycles()).Returns(default(IEnumerable<TestCycle>));
            var controller = PrepareController(this.testCycleServiceMock);

            var result = controller.Get();

            Assert.IsTrue(result.IsSuccessStatusCode, "Return successfull code");
            Assert.IsNotNull(result.Content, "TestCycleController.Get() return not null result");
        }

        [TestMethod]
        public void GetAllTestCyclesReturnValidItemsCount()
        {
            // arrange
            var testCycles = GetTestCycles();
            this.testCycleServiceMock.Setup(c => c.GetTestCycles()).Returns(testCycles);
            var controller = PrepareController(this.testCycleServiceMock);

            // act
            var responseMessage = controller.Get();
            var testCyclesResponse = ResponseHelper.GetContentFromResponseMessage<List<TestCycle>>(responseMessage);

            // assert
            Assert.IsNotNull(testCycles, "TestCycleController.Get() return not null result");
            Assert.AreEqual(testCycles.Count, testCyclesResponse.Count, "Test runs items count equal");
        }

        [TestMethod]
        public void GetAllTestCyclesReturnsInternalServerErrorCode()
        {
            this.testCycleServiceMock.Setup(c => c.GetTestCycles()).Throws(new Exception("Cannot get list of test runs."));
            var controller = PrepareController(this.testCycleServiceMock);

            var result = controller.Get();

            Assert.IsFalse(result.IsSuccessStatusCode, "Test cycle response hasn't success status code.");
            Assert.AreEqual(HttpStatusCode.InternalServerError, result.StatusCode, "Return HttpStatusCode.InternalServerError status code.");
        }

        #endregion tests

        #region Get by id - tests

        [TestMethod]
        public void GetWithIdNotFoundShouldReturnNotFoundStatusCode()
        {
            this.testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(default(TestCycle));
            var controller = PrepareController(this.testCycleServiceMock);

            var result = controller.Get(10);

            Assert.AreEqual(HttpStatusCode.NotFound, result.StatusCode, "TestCycle with id not found.");
            Assert.IsFalse(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void GetWithIdFoundShouldTestCycle()
        {
            var testCycleMock = this.GetTestCycleWithId();
            this.testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testCycleMock);
            var controller = PrepareController(this.testCycleServiceMock);

            var responseMessage = controller.Get(1);
            var testCycleResponse = ResponseHelper.GetContentFromResponseMessage<TestCycle>(responseMessage);

            Assert.AreEqual(HttpStatusCode.OK, responseMessage.StatusCode, "HttpStatusCode is OK");
            Assert.IsTrue(responseMessage.IsSuccessStatusCode, "Is IsSuccessStatusCode");
            Assert.IsNotNull(testCycleResponse, "TestCycle object in response in not null");
            Assert.AreEqual(testCycleMock.Comment, testCycleResponse.Comment);
            Assert.AreEqual(testCycleMock.Name, testCycleResponse.Name);
            Assert.AreEqual(testCycleMock.Id, testCycleResponse.Id);
        }

        #endregion

        #region CreateAndSave - tests

        [TestMethod]
        public void AddEmptyTestCycleReturnBadRequest()
        {
            var controller = PrepareController(this.testCycleServiceMock);

            var result = controller.Create(null);

            Assert.IsFalse(result.IsSuccessStatusCode, "Test run response hasn't success status code.");
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode, "Return HttpStatusCode.BadRequest status code.");
        }

        [TestMethod]
        public void AddTestCycleReturnInternalServerError()
        {
            var testCycleMock = this.GetTestCycleWithId();

            this.testCycleServiceMock.Setup(c => c.CreateAndSave(It.IsNotNull<TestCycle>())).Throws(new Exception("Cannot add test run."));
            var controller = PrepareController(this.testCycleServiceMock);

            var result = controller.Create(testCycleMock);

            Assert.IsFalse(result.IsSuccessStatusCode, "Test run response hasn't success status code.");
            Assert.AreEqual(HttpStatusCode.InternalServerError, result.StatusCode, "Return HttpStatusCode.BadRequest status code.");
        }

        [TestMethod]
        public void AddTestCycleReturnTestCycleAndStatusCreated()
        {
            var testCycleMock = this.GetTestCycleWithId();
            this.testCycleServiceMock.Setup(c => c.CreateAndSave(It.IsNotNull<TestCycle>())).Returns<TestCycle>(tr => tr);
            var controller = PrepareController(this.testCycleServiceMock);

            var responseMessage = controller.Create(testCycleMock);
            var testCycleResponse = ResponseHelper.GetContentFromResponseMessage<TestCycle>(responseMessage);

            Assert.IsTrue(responseMessage.IsSuccessStatusCode);
            Assert.AreEqual(HttpStatusCode.Created, responseMessage.StatusCode, "Return HttpStatusCode.Created status code.");
            Assert.AreEqual(testCycleMock.Comment, testCycleResponse.Comment);
            Assert.AreEqual(testCycleMock.Name, testCycleResponse.Name);
            Assert.AreEqual(testCycleMock.Id, testCycleResponse.Id);
        }

        #endregion

        #region Update - tests

        [TestMethod]
        public void UpdateWithNegativeIdReturnBadRequest()
        {
            var controller = PrepareController(this.testCycleServiceMock);

            var responseMessage = controller.Update(-1, null);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }

        [TestMethod]
        public void UpdateWithEmptyTestCycleReturnBadRequest()
        {
            var controller = PrepareController(this.testCycleServiceMock);

            var responseMessage = controller.Update(1, null);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }

        [TestMethod]
        public void UpdateWhereTestCycleNotAlreadyExistsReturnsBadRequest()
        {
            // arrange
            var testCycleMock = this.GetTestCycleWithId();
            this.testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(default(TestCycle));
            var controller = PrepareController(this.testCycleServiceMock);

            // act
            var responseMessage = controller.Update(1, testCycleMock);

            // assert
            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }


        [TestMethod]
        public void UpdateSuccessfulReturnsModifiedTestCycle()
        {
            // arrange
            var testCycleMock = this.GetTestCycleWithId();
            var testCycleToUpdate = this.GetTestCycleWithId();
            testCycleToUpdate.Comment = "Modified comment";
            this.testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testCycleMock);
            this.testCycleServiceMock.Setup(c => c.Update(It.IsNotNull<TestCycle>(), It.IsNotNull<TestCycle>())).Returns(testCycleToUpdate);
            var controller = PrepareController(this.testCycleServiceMock);

            // act
            var responseMessage = controller.Update(1, testCycleToUpdate);
            var testCycleResponse = ResponseHelper.GetContentFromResponseMessage<TestCycle>(responseMessage);

            // assert
            Assert.IsNotNull(responseMessage);
            Assert.IsTrue(responseMessage.IsSuccessStatusCode);
            Assert.AreEqual(HttpStatusCode.Accepted, responseMessage.StatusCode, "Return HttpStatusCode.Accepted status code.");
            Assert.AreEqual(testCycleToUpdate.Comment, testCycleResponse.Comment);
            Assert.AreEqual(testCycleToUpdate.Name, testCycleResponse.Name);
            Assert.AreEqual(testCycleToUpdate.Id, testCycleResponse.Id);
        }

        [TestMethod]
        public void UpdateReturnsNotModified()
        {
            // arrange
            var testCycleMock = this.GetTestCycleWithId();
            this.testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testCycleMock);
            this.testCycleServiceMock.Setup(c => c.Update(It.IsNotNull<TestCycle>(), It.IsNotNull<TestCycle>()))
                .Throws(new Exception("Cannot update TestCycle"));
            var controller = PrepareController(this.testCycleServiceMock);

            // act
            var responseMessage = controller.Update(1, testCycleMock);

            // assert
            Assert.IsNotNull(responseMessage);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
            Assert.AreEqual(HttpStatusCode.NotModified, responseMessage.StatusCode, "Return HttpStatusCode.NotModified status code.");
        }

        #endregion

        #region Delete - tests

        [TestMethod]
        public void DeleteNotExistedTestCycleReturnBadRequest()
        {
            this.testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(default(TestCycle));
            var controller = PrepareController(this.testCycleServiceMock);

            var responseMessage = controller.Delete(1);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }

        [TestMethod]
        public void DeleteTestCycleReturnOk()
        {
            var testCycleMock = this.GetTestCycleWithId();
            this.testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testCycleMock);
            this.testCycleServiceMock.Setup(c => c.Delete(It.IsAny<TestCycle>()));
            var controller = PrepareController(this.testCycleServiceMock);

            var responseMessage = controller.Delete(1);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.OK, responseMessage.StatusCode);
            Assert.IsTrue(responseMessage.IsSuccessStatusCode);
        }

        [TestMethod]
        public void DeleteTestCycleReturnNotModified()
        {
            var testCycleMock = this.GetTestCycleWithId();
            this.testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testCycleMock);
            this.testCycleServiceMock.Setup(c => c.Delete(It.IsAny<TestCycle>())).Throws(new Exception("Cannot delete specified test run."));
            var controller = PrepareController(this.testCycleServiceMock);

            var responseMessage = controller.Delete(1);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.NotModified, responseMessage.StatusCode);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }

        #endregion

        #region Helpers

        private static TestCycleController PrepareController(Mock<ITestCycleService> testCycleServiceMock)
        {
            var controller = new TestCycleController(testCycleServiceMock.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            return controller;
        }

        private TestCycle GetTestCycleWithId()
        {
            var testCycle = new TestCycle
            {
                Id = 1,
                Added = DateTime.Now,
                Modified = DateTime.Now,
                Comment = "Comment",
                Name = "Test cycle 1",
            };

            return testCycle;
        }

        private static List<TestCycle> GetTestCycles()
        {
            var testCycles = new List<TestCycle>()
                               {
                                   new TestCycle
                                       {
                                           Id = 1,
                                           Added = DateTime.Now,
                                           Modified = DateTime.Now,
                                           Comment = "Comment",
                                           Name = "Test cycle 1",
                                       },
                                   new TestCycle
                                       {
                                           Id = 2,
                                           Added = DateTime.Now,
                                           Modified = DateTime.Now,
                                           Comment = "Comment",
                                           Name = "Test cycle 2",
                                       },
                                    new TestCycle
                                       {
                                           Id = 3,
                                           Added = DateTime.Now,
                                           Modified = DateTime.Now,
                                           Comment = "Comment",
                                           Name = "Test cycle 3",
                                       }
                               };
            return testCycles;
        }

        #endregion
    }
}
