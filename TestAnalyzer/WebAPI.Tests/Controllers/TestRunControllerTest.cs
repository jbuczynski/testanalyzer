﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Tests.Controllers
{
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Mvc;
    using System.Xml;

    using Microsoft.AspNet.Identity;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Model;
    using Model.Enums;

    using Moq;

    using Service;
    using Service.Helpers;

    using WebAPI.Controllers;

    [TestClass]
    public class TestRunControllerTest
    {
        #region Fields - Mocks

        private Mock<ITestCycleService> _testCycleServiceMock;

        private Mock<ITestRunService> _testRunServiceMock;

        private Mock<IParserManagerService> _testParserServiceMock;

        private Mock<IFileManagerService> _fileRequestManagerMock;

        #endregion

        #region Set up

        /// <summary>
        /// The set up.
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            this._testCycleServiceMock = new Mock<ITestCycleService>();
            this._testRunServiceMock = new Mock<ITestRunService>();
            this._testParserServiceMock = new Mock<IParserManagerService>();
            this._fileRequestManagerMock = new Mock<IFileManagerService>();
        }

        #endregion

        #region Controller constructor - test

        [TestMethod]
        public void PropertyInitializeController()
        {
            try
            {
                var controller = PrepareController();

                Assert.IsNotNull(controller);
                Assert.IsInstanceOfType(controller, typeof(TestRunController));
            }
            catch (Exception ex)
            {
                Assert.Fail($"Cannot create instance of {nameof(TestRunController)}. Message: ", ex);
            }
        }

        #endregion

        #region Get all test runs - tests

        [TestMethod]
        public void GetAllTestRuns_ShouldReturnNotNull()
        {
            this._testRunServiceMock.Setup(c => c.GetTestRuns()).Returns(default(IEnumerable<TestRun>));
            var controller = PrepareController();

            var result = controller.Get();
            Assert.IsTrue(result.IsSuccessStatusCode, "Return successfull code");
            Assert.IsNotNull(result.Content, "TestRunController.Get() return not null result");
        }

        [TestMethod]
        public void GetAllTestRuns_ShouldReturnValidItemsCount()
        {
            // arrange
            var testRuns = GetTestListRun();
            this._testRunServiceMock.Setup(c => c.GetTestRuns()).Returns(testRuns);
            var controller = PrepareController();

            // act
            var responseMessage = controller.Get();
            var testRunsResponse = ResponseHelper.GetContentFromResponseMessage<List<TestRun>>(responseMessage);

            // assert
            Assert.IsNotNull(testRuns, "TestRunController.Get() return not null result");
            Assert.AreEqual(testRuns.Count, testRunsResponse.Count, "Test runs items count equal");
        }

        [TestMethod]
        public void GetAllTestRuns_ShouldReturnsInternalServerErrorCode()
        {
            this._testRunServiceMock.Setup(c => c.GetTestRuns()).Throws(new Exception("Cannot get list of test runs."));
            var controller = PrepareController();

            var result = controller.Get();

            Assert.IsFalse(result.IsSuccessStatusCode, "Test run response hasn't success status code.");
            Assert.AreEqual(HttpStatusCode.InternalServerError, result.StatusCode, "Return HttpStatusCode.InternalServerError status code.");
        }

        #endregion tests

        #region Get by id - tests

        [TestMethod]
        public void GetWithIdNotFound_ShouldReturnNotFoundStatusCode()
        {
            this._testRunServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(default(TestRun));
            var controller = PrepareController();

            var result = controller.Get(10);

            Assert.AreEqual(HttpStatusCode.NotFound, result.StatusCode, "TestRun with id not found.");
            Assert.IsFalse(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void GetWithIdFound_ShouldReturnTestRun()
        {
            var testRunMock = this.GetTestRunWithId();
            this._testRunServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testRunMock);
            var controller = PrepareController();

            var responseMessage = controller.Get(1);
            var testRunResponse = ResponseHelper.GetContentFromResponseMessage<TestRun>(responseMessage);

            Assert.AreEqual(HttpStatusCode.OK, responseMessage.StatusCode, "HttpStatusCode is OK");
            Assert.IsTrue(responseMessage.IsSuccessStatusCode, "Is IsSuccessStatusCode");
            Assert.IsNotNull(testRunResponse, "TestRun object in response in not null");
            Assert.AreEqual(testRunMock.Comment, testRunResponse.Comment);
            Assert.AreEqual(testRunMock.Name, testRunResponse.Name);
            Assert.AreEqual(testRunMock.UploadType, testRunResponse.UploadType);
            Assert.AreEqual(testRunMock.Id, testRunResponse.Id);
        }

        #endregion

        #region CreateAndSave - tests

        [TestMethod]
        public void AddEmptyTestRun_ShouldReturnBadRequest()
        {
            var controller = PrepareController();

            var result = controller.Create(null);

            Assert.IsFalse(result.IsSuccessStatusCode, "Test run response hasn't success status code.");
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode, "Return HttpStatusCode.BadRequest status code.");
        }

        [TestMethod]
        public void AddTestRun_ShouldReturnInternalServerError()
        {
            var testRunMock = this.GetTestRunWithId();

            this._testRunServiceMock.Setup(c => c.CreateAndSave(It.IsNotNull<TestRun>())).Throws(new Exception("Cannot add test run."));
            var controller = PrepareController();

            var result = controller.Create(testRunMock);

            Assert.IsFalse(result.IsSuccessStatusCode, "Test run response hasn't success status code.");
            Assert.AreEqual(HttpStatusCode.InternalServerError, result.StatusCode, "Return HttpStatusCode.BadRequest status code.");
        }

        [TestMethod]
        public void AddTestRun_ShouldReturnTestRunAndStatusCreated()
        {
            var testRunMock = this.GetTestRunWithId();
            this._testRunServiceMock.Setup(c => c.CreateAndSave(It.IsNotNull<TestRun>())).Returns<TestRun>(tr => tr);
            var controller = PrepareController();

            var responseMessage = controller.Create(testRunMock);
            var testRunResponse = ResponseHelper.GetContentFromResponseMessage<TestRun>(responseMessage);

            Assert.IsTrue(responseMessage.IsSuccessStatusCode);
            Assert.AreEqual(HttpStatusCode.Created, responseMessage.StatusCode, "Return HttpStatusCode.Created status code.");
            Assert.AreEqual(testRunMock.Comment, testRunResponse.Comment);
            Assert.AreEqual(testRunMock.Name, testRunResponse.Name);
            Assert.AreEqual(testRunMock.UploadType, testRunResponse.UploadType);
            Assert.AreEqual(testRunMock.Id, testRunResponse.Id);
        }

        #endregion

        #region Update - tests

        [TestMethod]
        public void UpdateWithNegativeId_ShouldReturnBadRequest()
        {
            var testRunMock = this.GetTestRunWithId();
            var controller = PrepareController();

            var responseMessage = controller.Update(-1, null);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode, "");
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }

        [TestMethod]
        public void UpdateWithEmptyTestRun_ShouldReturnBadRequest()
        {
            var testRunMock = this.GetTestRunWithId();
            var controller = PrepareController();

            var responseMessage = controller.Update(1, null);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }

        [TestMethod]
        public void UpdateWhereTestRunNotAlreadyExists_ShouldReturnsBadRequest()
        {
            // arrange
            var testRunMock = this.GetTestRunWithId();
            this._testRunServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(default(TestRun));
            var controller = PrepareController();

            // act
            var responseMessage = controller.Update(1, testRunMock);

            // assert
            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }


        [TestMethod]
        public void UpdateSuccessful_ShouldReturnsModifiedTestRun()
        {
            // arrange
            var testRunMock = this.GetTestRunWithId();
            var testRunToUpdate = this.GetTestRunWithId();
            testRunToUpdate.Comment = "Modified comment";
            this._testRunServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testRunMock);
            this._testRunServiceMock.Setup(c => c.Update(It.IsNotNull<TestRun>(), It.IsNotNull<TestRun>())).Returns(testRunToUpdate);
            var controller = PrepareController();

            // act
            var responseMessage = controller.Update(1, testRunToUpdate);
            var testRunResponse = ResponseHelper.GetContentFromResponseMessage<TestRun>(responseMessage);

            // assert
            Assert.IsNotNull(responseMessage);
            Assert.IsTrue(responseMessage.IsSuccessStatusCode);
            Assert.AreEqual(HttpStatusCode.Accepted, responseMessage.StatusCode, "Return HttpStatusCode.Accepted status code.");
            Assert.AreEqual(testRunToUpdate.Comment, testRunResponse.Comment);
            Assert.AreEqual(testRunToUpdate.Name, testRunResponse.Name);
            Assert.AreEqual(testRunToUpdate.UploadType, testRunResponse.UploadType);
            Assert.AreEqual(testRunToUpdate.Id, testRunResponse.Id);
        }

        [TestMethod]
        public void Update_ShouldReturnsNotModified()
        {
            // arrange
            var testRunMock = this.GetTestRunWithId();
            this._testRunServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testRunMock);
            this._testRunServiceMock.Setup(c => c.Update(It.IsNotNull<TestRun>(), It.IsNotNull<TestRun>()))
                .Throws(new Exception("Cannot update TestRun"));
            var controller = PrepareController();

            // act
            var responseMessage = controller.Update(1, testRunMock);
            var testRunResponse = ResponseHelper.GetContentFromResponseMessage<TestRun>(responseMessage);

            // assert
            Assert.IsNotNull(responseMessage);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
            Assert.AreEqual(HttpStatusCode.NotModified, responseMessage.StatusCode, "Return HttpStatusCode.NotModified status code.");
        }

        #endregion

        #region Delete - tests

        [TestMethod]
        public void DeleteNotExistedTestRun_ShouldReturnBadRequest()
        {
            this._testRunServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(default(TestRun));
            var controller = PrepareController();

            var responseMessage = controller.Delete(1);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }

        [TestMethod]
        public void DeleteTestRun_ShouldReturnOk()
        {
            var testRunMock = this.GetTestRunWithId();
            this._testRunServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testRunMock);
            this._testRunServiceMock.Setup(c => c.Delete(It.IsAny<TestRun>()));
            var controller = PrepareController();

            var responseMessage = controller.Delete(1);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.OK, responseMessage.StatusCode);
            Assert.IsTrue(responseMessage.IsSuccessStatusCode);
        }

        [TestMethod]
        public void DeleteTestRun_ShouldReturnNotModified()
        {
            var testRunMock = this.GetTestRunWithId();
            this._testRunServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(testRunMock);
            this._testRunServiceMock.Setup(c => c.Delete(It.IsAny<TestRun>())).Throws(new Exception("Cannot delete specified test run."));
            var controller = PrepareController();

            var responseMessage = controller.Delete(1);

            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(HttpStatusCode.NotModified, responseMessage.StatusCode);
            Assert.IsFalse(responseMessage.IsSuccessStatusCode);
        }

        #endregion

        #region PostXml - tests

        [TestMethod]
        [TestCategory("PostXml")]
        public void PostXmlNoRequestFile_ShouldReturnUnsupporterdMediaType()
        {
            var controller = PrepareController();

            var controllerContext = new HttpControllerContext
            {
                Request = new HttpRequestMessage
                {
                    Content = new ObjectContent(typeof(string), "content", new JsonMediaTypeFormatter())
                }
            };

            controller.ControllerContext = controllerContext;

            var result = ResponseHelper.GetHttpResponseMessageFromTask(controller.PostXml());

            Assert.AreEqual(HttpStatusCode.UnsupportedMediaType, result.StatusCode);
        }

        [TestMethod]
        [TestCategory("PostXml")]
        public void PostXmlInvalidTestCycleIdNumber_ShouldReturnBadRequest()
        {
            this._fileRequestManagerMock.Setup(c => c.ReadFromRequest(It.IsAny<HttpRequestMessage>())).ReturnsAsync(new TestRunMultipartFormDataStreamProvider("path"));
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("testCycleId"))).Returns(new[] { "abc" });

            var controller = this.GetPreparedControllerForPostXmlTests();

            var responseMessage = ResponseHelper.GetHttpResponseMessageFromTask(controller.PostXml());
            var httpError = ResponseHelper.GetContentFromResponseMessage<HttpError>(responseMessage);

            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.AreEqual("Cant convert testCycleId to number", httpError.Message);
        }

        [TestMethod]
        [TestCategory("PostXml")]
        public void PostXmlTestCycleIdNotExists_ShouldReturnBadRequest()
        {
            this._fileRequestManagerMock.Setup(c => c.ReadFromRequest(It.IsAny<HttpRequestMessage>())).ReturnsAsync(new TestRunMultipartFormDataStreamProvider("path"));
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("testCycleId"))).Returns(new[] { "1" });
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("comment"))).Returns(new[] { "comment" });
            this._fileRequestManagerMock.Setup(c => c.ProviderContaintFileData()).Returns(true);
            this._fileRequestManagerMock.Setup(c => c.GetXmlDocument()).Returns(new XmlDocument());

            this._testParserServiceMock.Setup(c => c.TryParse(It.IsAny<XmlDocument>(), It.IsAny<string>(), TestSuitsType.Unknow)).Returns(new TestRun());
            this._testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(default(TestCycle));

            var controller = this.GetPreparedControllerForPostXmlTests();

            var responseMessage = ResponseHelper.GetHttpResponseMessageFromTask(controller.PostXml());
            var httpError = ResponseHelper.GetContentFromResponseMessage<HttpError>(responseMessage);

            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.AreEqual("Test cycle with id = 1 does not exist.", httpError.Message);
        }

        [TestMethod]
        [TestCategory("PostXml")]
        public void PostXmlNoFile_ShouldReturnBadRequest()
        {
            this._fileRequestManagerMock.Setup(c => c.ReadFromRequest(It.IsAny<HttpRequestMessage>())).ReturnsAsync(new TestRunMultipartFormDataStreamProvider("path"));
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("testCycleId"))).Returns(new[] { "1" });
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("comment"))).Returns(new[] { "comment" });

            var controller = this.GetPreparedControllerForPostXmlTests();

            var responseMessage = ResponseHelper.GetHttpResponseMessageFromTask(controller.PostXml());
            var httpError = ResponseHelper.GetContentFromResponseMessage<HttpError>(responseMessage);

            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.AreEqual("Xml file is missing", httpError.Message);
        }

        [TestMethod]
        [TestCategory("PostXml")]
        public void PostXmlWithLocalFileNameNotExits_ShouldReturnBadRequest()
        {
            this._fileRequestManagerMock.Setup(c => c.ReadFromRequest(It.IsAny<HttpRequestMessage>())).ReturnsAsync(new TestRunMultipartFormDataStreamProvider("path"));
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("testCycleId"))).Returns(new[] { "1" });
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("comment"))).Returns(new[] { "comment" });
            this._fileRequestManagerMock.Setup(c => c.ProviderContaintFileData()).Returns(true);
            this._fileRequestManagerMock.Setup(c => c.GetLocalFileName()).Returns(string.Empty);

            var controller = this.GetPreparedControllerForPostXmlTests();

            var responseMessage = ResponseHelper.GetHttpResponseMessageFromTask(controller.PostXml());
            var httpError = ResponseHelper.GetContentFromResponseMessage<HttpError>(responseMessage);

            Assert.AreEqual(HttpStatusCode.BadRequest, responseMessage.StatusCode);
            Assert.IsFalse(string.IsNullOrEmpty(httpError.Message));
        }

        [TestMethod]
        [TestCategory("PostXml")]
        public void PostXmlWithCantParserXml_ShouldReturnNotAcceptable()
        {
            this._testParserServiceMock.Setup(c => c.TryParse(It.IsAny<XmlDocument>(), It.IsAny<string>(), TestSuitsType.Unknow)).Returns(default(TestRun));
            this._fileRequestManagerMock.Setup(c => c.ReadFromRequest(It.IsAny<HttpRequestMessage>())).ReturnsAsync(new TestRunMultipartFormDataStreamProvider("path"));
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("testCycleId"))).Returns(new[] { "1" });
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("comment"))).Returns(new[] { "comment" });
            this._fileRequestManagerMock.Setup(c => c.ProviderContaintFileData()).Returns(true);
            this._fileRequestManagerMock.Setup(c => c.GetXmlDocument()).Returns(new XmlDocument());

            var controller = this.GetPreparedControllerForPostXmlTests();

            var responseMessage = ResponseHelper.GetHttpResponseMessageFromTask(controller.PostXml());
            var httpError = ResponseHelper.GetContentFromResponseMessage<HttpError>(responseMessage);

            Assert.AreEqual(HttpStatusCode.NotAcceptable, responseMessage.StatusCode);
            Assert.AreEqual("Couldn't parse xml to TestRun object", httpError.Message);
        }

        [TestMethod]
        [TestCategory("PostXml")]
        public void PostXml_ShouldReturnOkCode()
        {
            this._fileRequestManagerMock.Setup(c => c.ReadFromRequest(It.IsAny<HttpRequestMessage>())).ReturnsAsync(new TestRunMultipartFormDataStreamProvider("path"));
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("testCycleId"))).Returns(new[] { "1" });
            this._fileRequestManagerMock.Setup(c => c.ReadValues(It.IsRegex("comment"))).Returns(new[] { "comment" });
            this._fileRequestManagerMock.Setup(c => c.ProviderContaintFileData()).Returns(true);
            this._fileRequestManagerMock.Setup(c => c.GetXmlDocument()).Returns(XmlHelper.LoadXmlDocument(TestDataHelper.NUnitPath));

            this._testParserServiceMock.Setup(c => c.TryParse(It.IsAny<XmlDocument>(), It.IsAny<string>(), TestSuitsType.Unknow)).Returns(new TestRun());
            this._testCycleServiceMock.Setup(c => c.GetById(It.IsAny<int>())).Returns(new TestCycle() { TestsRuns = new List<TestRun>(), Id = 1 });
            this._testRunServiceMock.Setup(c => c.CreateAndSave(It.IsNotNull<TestRun>())).Returns<TestRun>(FillTestRunAfterSave);

            var controller = this.GetPreparedControllerForPostXmlTests();

            var response = ResponseHelper.GetHttpResponseMessageFromTask(controller.PostXml());

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        #endregion

        #region Helpers

        private TestRunController GetPreparedControllerForPostXmlTests()
        {
            var controller = this.PrepareController();

            var content = new ByteArrayContent(new byte[0]);
            content.Headers.Add("Content-Disposition", "form-data");
            var controllerContext = new HttpControllerContext
            {
                Request = new HttpRequestMessage
                {
                    Content = new MultipartContent { content },
                    Method = HttpMethod.Post,
                }
            };

            controller.ControllerContext = controllerContext;
            controller.Request.SetConfiguration(new HttpConfiguration());

            return controller;
        }

        private TestRunController PrepareController()
        {
            var controller = new TestRunController(this._testCycleServiceMock.Object,
                                                   this._testRunServiceMock.Object,
                                                   this._testParserServiceMock.Object,
                                                   this._fileRequestManagerMock.Object);

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            return controller;
        }

        private TestRun GetTestRunWithId()
        {
            var testRun = new TestRun
            {
                Id = 1,
                Added = DateTime.Now,
                Modified = DateTime.Now,
                Comment = "Comment",
                Name = "Test cycle 1",
                UploadType = UploadType.Ci,
            };

            return testRun;
        }

        private static List<TestRun> GetTestListRun()
        {
            var testRuns = new List<TestRun>()
                               {
                                   new TestRun
                                       {
                                           Added = DateTime.Now,
                                           Modified = DateTime.Now,
                                           Comment = "Comment",
                                           Name = "Test cycle 1",
                                           UploadType = UploadType.Ci,
                                       }
                               };
            return testRuns;
        }

        private static TestRun FillTestRunAfterSave(TestRun testRun)
        {
            testRun.User = new ApplicationUser() { Id = "123", UserName = "testUser", Email = "testUser@email.com" };
            testRun.Id = 1;
            testRun.Added = DateTime.Now;
            testRun.Modified = DateTime.Now;
            testRun.UploadType = UploadType.Ci;

            return testRun;
        }

        #endregion
    }
}
